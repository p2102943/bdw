<div class="page">
    <div class="titre_page">
        <h1>Gestion des inscriptions</h1>
    </div>
    <?php
        if($message == "Il n'y a pas d'adhérant inscrits dans cette école.")
        {
    ?>
            <div class="msg_resultat_ins">
                <p><?= $message ?></p>
            </div>
    <?php
        }
        else
        { // Partie où les adhérants sont afficher avec la barre de recherche.
        
    ?>
        <?php
            if ( !isset($_GET['identifiant']))
            { ?>
                <div class="filtre"> 
                    <form class="filtre" action="#" method="post">
                        <h2> Filtrer par </h2>
                        <select name="attributRecherche" id="attributRecherche">
                            <?php foreach($adherant['schema'] as $att){ ?>
                                <option value="<?= $att['nom'] ?>"><?= att_to_nom($att['nom']) ?></option>
                            <?php }?>
                        </select>
                        <input type="text" name="mots_cles" placeholder="Entrez la recherche"/>
                        <input type="submit" name="boutonRechercher" value="Rechercher"/>
                    </form>
                </div>
            <?php } ?>
        <?php
        if(count($adherant['instances']) != 0)
        {
        ?>
            <div class="msg_resultat_ins">
                <p>
                    <?= $message; ?>
                </p>
            </div>
            <div class="res_ins">
                <table class="table_resultat">
                        <thead>
                            <tr>
                            <?php
                                //var_dump($resultats);
                                foreach($adherant['schema'] as $att) {  // pour parcourir les attributs
                        
                                    echo '<th>';
                                        echo att_to_nom($att['nom']);
                                    echo '</th>';
                        
                                }
                                if( !isset($_GET['identifiant'])){echo '<th></th>';}
                            ?>	
                            </tr>	
                            </thead>
                        <tbody>

                        <?php
                        
                            foreach($adherant['instances'] as $row) {  // pour parcourir les n-uplets
                        
                            echo '<tr>';
                            $keys = array_keys($row);
                            if( !isset($_GET['identifiant'])){array_push($row,"Visualiser");}
                            foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                if($valeur == 'Visualiser')
                                {
                                    echo '<td>'. '<a href="./index.php?page=danse_inscription&idEcole='.$_GET['idEcole'].'&identifiant='.$row[$keys[0]].'">Visualiser</a>' . '</td>';
                                }
                                else
                                {
                                    echo '<td>'. $valeur . '</td>';
                                }
                            }
                            echo '</tr>';
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <!-- 
            <div style="padding: 20px;">
                <ul class="links">
                    <li><a href="index.php?page=danse_inscription&idEcole=<?= $_GET['idEcole']?>&ajouterAd=existant">Ajouter un adhérant existant</a></li>
                    <li><a href="index.php?page=danse_inscription&idEcole=<?= $_GET['idEcole']?>&ajouterAd=non_existant">Ajouter un adhérant non existant</a></li>
                </ul>
            </div> -->
            <?php
            }
            else
            {
            ?>
            <div class="msg_resultat_ins">
                    <p><?= $message ?></p>
            </div>
            <?php
            }
            if(isset($_GET['identifiant']))
            { ?>
            <div style="display: flex; justify-content: space-around; padding: 30px;">
                <div class="bloc_cours">
                    <div class="msg_resultat_ins">
                        <p><?= (count($cours_ad['instances']) == 0 ? "Pas de cours enregistré.": $message_ad) ?></p>
                    </div>
                    <?php 
                    if(count($cours_ad['instances']) != 0)
                    { ?>
                        <div class="res_ins">
                            <table class="table_resultat">
                                <thead>
                                    <tr>
                                    <?php
                                            //var_dump($resultats);
                                            foreach($cours_ad['schema'] as $att) {  // pour parcourir les attributs
                                    
                                                echo '<th>';
                                                    echo att_to_nom($att['nom']);
                                                echo '</th>';
                                    
                                            }
                                            echo '<th></th>'
                                        ?>	
                                        </tr>	
                                        </thead>
                                    <tbody>

                                    <?php
                                    
                                        foreach($cours_ad['instances'] as $row) {  // pour parcourir les n-uplets
                                    
                                        echo '<tr>';
                                        $keys = array_keys($row);
                                        array_push($row,"Supprimer");
                                        foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                            if($valeur == 'Supprimer')
                                            {
                                                echo '<td>'. '<a href="./index.php?page=danse_inscription&idEcole='.$_GET['idEcole'].'&identifiant='.$_GET['identifiant'].'&idCours='.$row['idCours'].'&supprimer=#">Supprimer</a>' . '</td>';
                                            }
                                            else
                                            {
                                                echo '<td>'. $valeur . '</td>';
                                            }
                                        }
                                        echo '</tr>';
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
                <div class="bloc_cours">
                    <div class="msg_resultat_ins">
                        <p><?= $message_cours ?></p>
                    </div>
                    <?php
                    if(count($cours['instances']) != 0)
                    { ?>
                        <div class="res_ins">
                            <table class="table_resultat">
                                <thead>
                                    <tr>
                                    <?php
                                            //var_dump($resultats);
                                            foreach($cours['schema'] as $att) {  // pour parcourir les attributs
                                    
                                                echo '<th>';
                                                    echo att_to_nom($att['nom']);
                                                echo '</th>';
                                    
                                            }
                                            echo '<th></th>'
                                        ?>	
                                        </tr>	
                                        </thead>
                                    <tbody>

                                    <?php
                                    
                                        foreach($cours['instances'] as $row) {  // pour parcourir les n-uplets
                                    
                                        echo '<tr>';
                                        $keys = array_keys($row);
                                        array_push($row,"Ajouter");
                                        foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                            if($valeur == 'Ajouter')
                                            {
                                                echo '<td>'. '<a href="./index.php?page=danse_inscription&identifiant='.$_GET['identifiant']."&idCours=".$row['idCours']."&idEcole=".$row['idE'].'">Ajouter</a>' . '</td>';
                                            }
                                            else
                                            {
                                                echo '<td>'. $valeur . '</td>';
                                            }
                                        }
                                        echo '</tr>';
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
    <?php 
        }
    ?>
</div>
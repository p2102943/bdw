<div class="page">
    <div class="titre_page">
        <h1>Gestion des membres</h1>
    </div>
    <?php if($message != "Cette fédération ne contient pas de comité."){ // Donc pas vide ?> 
        <?php if(!isset($_GET['ajouter'])){
                if(!isset($_GET['idMem']) && isset($_GET['comite'])){ ?>
                    <ul class="links">
                        <li><a href="index.php?page=federation_membre&idF=<?= $_GET['idF'] ?>&comite=<?= $_GET['comite'] ?>&Selec=Choisir&ajouter=membre">Ajouter un membre au comité</a></li>
                    </ul>
                <?php } ?>
        <?php if(!isset($membre)) { ?>
            <h1> <?= $message ?></h1>
            <form action="#" class="form_accueil" method="get">
                <input name="page" type="hidden" value="federation_membre">
                <input name="idF" type="hidden" value="<?= $_GET['idF'] ?>">
                <select name="comite" id="comite">
                    <?php foreach($comite['instances'] as $row){?>
                        <option value="<?= $row['idC'] ?>"><?= $row['nomComité'] ?> <?= $row['niveau'] ?></option>
                    <?php }?>
                </select>
                <input type="submit" value="Choisir" name="Selec">
            </form>
            <div class = "retour">
                <a href="index.php?page=federation_accueil&idF=<?= $_GET['idF'] ?>&Valider=Valider#"> Retour </a>
            </div>
        <?php }else{ ?>
            <h2><?= $message_m ?></h2>
            <?php if(isset($_GET['supprimer'])){echo "</br>  <h2>".$message_supp."</h2>";} ?>
            <?php if(count($membre['instances']) != 0){ ?>
                <div class="membre">
                    <table class="table_resultat">
                                <thead>
                                    <tr>
                                    <?php
                                        foreach($membre['schema'] as $att) {  // pour parcourir les attributs
                                
                                            echo '<th>';
                                                echo att_to_nom($att['nom']);
                                            echo '</th>';
                                
                                        }
                                        if( !isset($_GET['idMem'])){echo '<th> Cliquez pour visualiser </th>';echo '<th>Cliquez pour supprimer</th>';}
                                    ?>	
                                    </tr>	
                                    </thead>
                                <tbody>

                                <?php
                                
                                    foreach($membre['instances'] as $row) {  // pour parcourir les n-uplets
                                
                                    echo '<tr>';
                                    $keys = array_keys($row);
                                    if(!isset($_GET['idMem'])){array_push($row,"Visualiser");array_push($row,"Supprimer");};
                                    foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                        if($valeur == 'Visualiser')
                                        {
                                            echo '<td>'. '<a href="./index.php?page=federation_membre&idF='.$_GET['idF'].'&comite='.$_GET['comite'].'&Selec=Choisir&idMem='.$row[$keys[0]].'&visualiser=membre">Visualiser</a>' . '</td>';
                                        }
                                        else if($valeur == 'Supprimer')
                                        {
                                            echo '<td>'. '<a href="./index.php?page=federation_membre&idF='.$_GET['idF'].'&comite='.$_GET['comite'].'&Selec=Choisir&idSupp='.$row[$keys[0]].'&supprimer=membre">Supprimer</a>' . '</td>';

                                        }
                                        else 
                                        {
                                            echo '<td>'. $valeur . '</td>';
                                        }
                                    }
                                    echo '</tr>';
                                }
                            ?>
                            </tbody>
                        </table>
                    <?php if(!isset($_GET['idMem'])) { ?>
                        <div class = "retour">
                            <a href="index.php?page=federation_membre&idF=<?= $_GET['idF'] ?>"> Retour </a>
                        </div>
                    <?php } ?>
                </div>
            <?php }if(isset($_GET['visualiser'])) {
                    if($_GET['visualiser'] == "membre"){ ?>
                <div class="modif">
                    <h1>Modification des valeurs du membre : </h1>
                    </br>
                    <form action="#" class="form_accueil" method="get">
                        <input name="page" type="hidden" value="federation_membre">
                        <input name="idF" type="hidden" value="<?= $_GET['idF'] ?>">
                        <input name="comite" type="hidden" value="<?= $_GET['comite'] ?>">
                        <input name="visualiser" type="hidden" value="membre">
                        <input name="Selec" type="hidden" value="Choisir">
                        <input name="idMem" type="hidden" value="<?= $_GET['idMem'] ?>">
                        
                        <label for="nom">Nom : </label><input type="text" name="nom" placeholder="Entrez le nom" value=" <?= $membre['instances'][0]['nom'] ?>"/>
                        <label for="prenom">Prénom : </label><input type="text" name="prenom" placeholder="Entrez le prénom" value=" <?= $membre['instances'][0]['prénom'] ?>"/>
                        </br>
                        </br>
                        <label for="fonction">Fonction : </label><input type="text" name="fonction" placeholder="Entrez la fonction" value=" <?= $membre['instances'][0]['fonction'] ?>"/>
                    </br>
                </br>


                        <label for="jour">Jour : </label>
                        <select name="jour" id="jour">
                            <option value="<?= $date_prec[0] ?>"><?= $date_prec[0] ?></option>
                            <?php for($i = 1 ; $i <= 31 ; $i++){ ?>
                                <?php if($i != (int) $date_prec[0]){?><option value="<?= $i ?>"><?= $i ?></option> <?php } ?>
                            <?php }?>
                        </select>
                        <label for="mois">Mois : </label>
                        <select name="mois" id="mois">
                            <option value="<?= $date_prec[1] ?>"><?= $date_prec[1] ?></option>
                            <?php for($i = 1 ; $i <= 12 ; $i++){ ?>
                                <?php if($i != (int) $date_prec[1]){?><option value="<?= $i ?>"><?= $i ?></option> <?php } ?>
                            <?php }?>
                        </select>
                        <label for="annee">Année : </label>
                        <select name="annee" id="annee">
                            <option value="<?= $date_prec[2] ?>"><?= $date_prec[2] ?></option>
                            <?php for($i = 1950 ; $i <= 2005; $i++){ ?>
                                <?php if($i != (int) $date_prec[2]){?><option value="<?= $i ?>"><?= $i ?></option> <?php } ?>
                            <?php }?>
                        </select>
                            </br>
                            </br>
                            </br>
                        <input type="submit" value="Modifier" name="ModifierComite">
                    </form>
                    <div class="msg_resultat_ins">
                        <h1><?=$message_err?></h1>
                    </div> 
                    <div class = "retour">
                        <a href="index.php?page=federation_membre&idF=<?= $_GET['idF'] ?>&comite=<?= $_GET['comite'] ?>&Selec=Choisir"> Retour </a>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
            <?php } ?>
        <?php } if(isset($_GET['ajouter'])) {
                    if($_GET['ajouter'] == "membre") {?>
                <div class="ajout">
                    <h1>Ajout d'un membre : </h1>
                    </br>
                    <form action="#" class="form_accueil" method="post">
                        <label for="nom">Nom : </label><input type="text" name="nom" placeholder="Entrez le nom" />
                        <label for="prenom">Prénom : </label><input type="text" name="prenom" placeholder="Entrez le prénom"     />                   
                        </br>
                        </br>
                        <label for="fonction">Fonction : </label><input type="text" name="fonction" placeholder="Entrez la fonction" />
                        </br>
                        </br>


                        <label for="jour">Jour : </label>
                        <select name="jour" id="jour">
                            <?php for($i = 1 ; $i <= 31 ; $i++){ ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                            <?php }?>
                        </select>
                        <label for="mois">Mois : </label>
                        <select name="mois" id="mois">
                            <?php for($i = 1 ; $i <= 12 ; $i++){ ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                            <?php }?>
                        </select>
                        <label for="annee">Année : </label>
                        <select name="annee" id="annee">
                            <?php for($i = 1950 ; $i <= 2005; $i++){ ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                            <?php }?>
                        </select>
                        </br>
                        </br>
                        <input type="submit" name="ajouterMembre" value="Ajouter"/>
                    </form>
                        </br>
                        <div class="msg_resultat_ins">
                        <p>
                            <?= $message_err ?>
                        </p>
                        </div>
                        </br>
                        <div class = "retour">
                            <a href="index.php?page=federation_membre&idF=<?= $_GET['idF'] ?>&comite=<?= $_GET['comite'] ?>&Selec=Choisir"> Retour </a>
                        </div>
                </div>
            <?php } ?>  
        <?php } ?>  
    <?php } ?>
</div>
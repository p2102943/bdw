<div class="accueil">
    <div class="titre_page">
        <h1>Voici quelques statistiques globales... </h1>
    </div>
    <div class="stats_accueil">
        <div class="bloc_stats">
            <p> Nombre de fédérations :</p>
            <h1 style="color: #cb88ff;"><?=$nbFede?></h1>
            <p> Nombre de comités regionaux :</p>
            <h1 style="color: #cb88ff;"><?=$nbCR?></h1>
            <p> Nombre de comités départementales :</p>
            <h1 style="color: #cb88ff;"><?=$nbCD?></h1>
        </div>
        <div class="bloc_stats">
            <p>Voici les 5 écoles avec le plus d'adhérents</p>
            <?php
            $i = 1;
            foreach($ecole['instances'] as $row){
                echo '<p style="color: #cb88ff;font-size:small;">'.$i.' - ';
                foreach($row as $att){
                    echo $att." ";
                }
                $i++;
                echo '</p>';
            }
            ?>
        </div>
        <div class="bloc_stats">
            <p>Voici le nombre d'école par code de département français </p>
                <?php if(count($nbEcoleDeptFR['instances']) != 0)
                {?>
                <table class="table_resultat">
                        <thead>
                            <tr>
                            <?php
                                //var_dump($resultats);
                                foreach($nbEcoleDeptFR['schema'] as $att) {  // pour parcourir les attributs
                        
                                    echo '<th>';
                                        echo att_to_nom($att['nom']);
                                    echo '</th>';
                        
                                }
                            ?>	
                            </tr>	
                            </thead>
                        <tbody>

                        <?php
                        
                            foreach($nbEcoleDeptFR['instances'] as $row) {  // pour parcourir les n-uplets
                        
                            echo '<tr>';
                            foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                    echo '<td>'. $valeur . '</td>';
                            }
                            echo '</tr>';
                        }
                    ?>
                    </tbody>
                </table>
                <?php }?>
        </div>
        <div class="bloc_stats">
            <p>Voici le nombre d'école par code de département français </p>
            <?php if(count($comiteReg['instances']) != 0)
            {?>
            <table class="table_resultat">
                    <thead>
                        <tr>
                        <?php
                            //var_dump($resultats);
                            foreach($comiteReg['schema'] as $att) {  // pour parcourir les attributs
                    
                                echo '<th>';
                                    echo att_to_nom($att['nom']);
                                echo '</th>';
                    
                            }
                        ?>	
                        </tr>	
                        </thead>
                    <tbody>

                    <?php
                    
                        foreach($comiteReg['instances'] as $row) {  // pour parcourir les n-uplets
                    
                        echo '<tr>';
                        foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                echo '<td>'. $valeur . '</td>';
                        }
                        echo '</tr>';
                    }
                ?>
                </tbody>
            </table>
            <?php }?>
        </div>
    </div>
</div>
<?php
if($message == "Erreur !")
{ ?>
    <div class="accueil">  
    <div class="titre_page">
        <h1><?= $message ?> </h1>
    </div>

<?php
} else {
    if( !isset($_GET['Valider']))
    {
?>


<div class="accueil">  
    <div class="titre_page">
        <h1><?= $message ?> </h1>
    </div>
    <form action="#" class="form_accueil" method="get">
        <input name="page" type="hidden" value="danse_accueil">
        <select name="idEcole" id="idEcole">
            <?php foreach($responsables['instances'] as $row){?>
                <option value="<?= $row['idE'] ?>"><?= $row['fondateurEcole'] ?></option>
            <?php }?>
        </select>
        <input type="submit" value="Valider" name="Valider">
    </form>
</div>

<?php }
    else{?>
    <div class="ecole">
        <div class="info_accueil">
            <h2> <?= $nomEcole ?> </h2>
            <h2>Adresse : 
                <?php
                    foreach($info_ecole['instances'] as $row){
                        foreach($row as $att){
                            if($att != $nomEcole)
                            {echo $att." ";}
                }}?>
            </h2>
        </div>
        <div class="titre_page">
            <h1>Tableau de bord de danse</h1>
        </div>
        <div style="padding: 20px;">
            <h2>Accéder à :</h2>
            <ul class="links">
                <li><a href="index.php?page=danse_inscription&idEcole=<?= $_GET['idEcole']?>">Gestion des employés de l’école</a></li>
                <li><a href="index.php?page=danse_inscription&idEcole=<?= $_GET['idEcole']?>">Gestion des cours</a></li>
                <li><a href="index.php?page=danse_inscription&idEcole=<?= $_GET['idEcole']?>">Gestion des adhérents de l’école</a></li>
                <li><a href="index.php?page=danse_inscription&idEcole=<?= $_GET['idEcole']?>">Gestion des inscriptions</a></li>
            </ul>
        </div>
        <div class="stats_accueil">
            <div class="bloc_stats"> <!-- Liste des employés -->
                <p><?= $message_emp ?></p>
                <?php if(count($employe['instances']) != 0)
                {?>
                <table class="table_resultat">
                        <thead>
                            <tr>
                            <?php
                                //var_dump($resultats);
                                foreach($employe['schema'] as $att) {  // pour parcourir les attributs
                        
                                    echo '<th>';
                                        echo att_to_nom($att['nom']);
                                    echo '</th>';
                        
                                }
                            ?>	
                            </tr>	
                            </thead>
                        <tbody>

                        <?php
                        
                            foreach($employe['instances'] as $row) {  // pour parcourir les n-uplets
                        
                            echo '<tr>';
                            foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                    echo '<td>'. $valeur . '</td>';
                            }
                            echo '</tr>';
                        }
                    ?>
                    </tbody>
                </table>
                <?php }?>
            </div>

            <div class="bloc_stats"> <!-- Nombre d'adhérants -->
                <p>Voici le nombre d'adhérants isncrits pour l'année 2022</p>
                <h1 style="color: #cb88ff;"><?= $nbAd?></h1>
            </div>

            <div class="bloc_stats"> <!-- Liste des cours -->
                <p> <?= $message_cours ?></p>
                <?php if(count($cours['instances']) != 0)
                {?>
                <table class="table_resultat">
                        <thead>
                            <tr>
                            <?php
                                //var_dump($resultats);
                                foreach($cours['schema'] as $att) {  // pour parcourir les attributs
                        
                                    echo '<th>';
                                        echo att_to_nom($att['nom']);
                                    echo '</th>';
                        
                                }
                            ?>	
                            </tr>	
                            </thead>
                        <tbody>

                        <?php
                        
                            foreach($cours['instances'] as $row) {  // pour parcourir les n-uplets
                        
                            echo '<tr>';
                            foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                    echo '<td>'. $valeur . '</td>';
                            }
                            echo '</tr>';
                        }
                    ?>
                    </tbody>
                </table>
                <?php }?>
            </div>

            <div class="bloc_stats">
                <p>Voici le nombre d'adhérents participants à une compétition au total</p>
                <h1 style="color: #cb88ff;"><?= $nbAdCompet ?></h1>
            </div>

        </div>
    </div>
<?php
    }}?>
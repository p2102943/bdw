<div class="page">
    <div class="titre_page">
        <h1>Gestion des compétitions</h1>
    </div>
    <?php
        if($message == "Il n'y a pas d'adhérant inscrits dans cette école.")
        {
    ?>
            <div class="msg_resultat_ins">
                <p><?= $message ?></p>
            </div>
    <?php
        }
        else
        { // Partie où les adhérants sont afficher avec la barre de recherche.
        
    ?>
        <?php
            if ( !isset($_GET['idCom']) && !isset($_GET['ajouter']))
            { ?>
                <ul class="links">
                    <li><a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&ajouter=compet">Ajouter une nouvelle compétition</a></li>
                </ul>
                <div class="filtre"> 
                    <form class="filtre" action="#" method="post">
                        <h2> Filtrer par </h2>
                        <select name="attributRecherche" id="attributRecherche">
                            <?php foreach($competition['schema'] as $att){ ?>
                                <option value="<?= $att['nom'] ?>"><?= att_to_nom($att['nom']) ?></option>
                            <?php }?>
                        </select>
                        <input type="text" name="mots_cles" placeholder="Entrez la recherche"/>
                        <input type="submit" name="boutonRechercher" value="Rechercher"/>
                    </form>
                </div>
            <?php } ?>
            <div class="msg_resultat_ins">
                <p>
                    <?= $message ?>
                </p>
            </div>
            <?php if(count($competition['instances']) != 0 && !isset($_GET['ajouter'])){ ?>
                <div class="res_com">
                    <table class="table_resultat">
                            <thead>
                                <tr>
                                <?php
                                    foreach($competition['schema'] as $att) {  // pour parcourir les attributs
                            
                                        echo '<th>';
                                            echo att_to_nom($att['nom']);
                                        echo '</th>';
                            
                                    }
                                    if( !isset($_GET['idCom'])){echo '<th> Cliquez pour visualiser </th>';}
                                ?>	
                                </tr>	
                                </thead>
                            <tbody>

                            <?php
                            
                                foreach($competition['instances'] as $row) {  // pour parcourir les n-uplets
                            
                                echo '<tr>';
                                $keys = array_keys($row);
                                if(!isset($_GET['idCom'])){array_push($row,"Visualiser");};
                                foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                    if($valeur == 'Visualiser')
                                    {
                                        echo '<td>'. '<a href="./index.php?page=federation_compet&idF='.$_GET['idF'].'&idCom='.$row[$keys[0]].'">Visualiser</a>' . '</td>';
                                    }
                                    else 
                                    {
                                        echo '<td>'. $valeur . '</td>';
                                    }
                                }
                                echo '</tr>';
                            }
                        ?>
                        </tbody>
                    </table>
                </div>




            <?php }
            if(isset($_GET['idCom']))
            { 
                if(!isset($_GET['modifier']) && !isset($_GET['ajouter']) && !isset($_GET['inscrire']) && !isset($_GET['affecter'])){ // Bloc d'accueil d'une compétition selectionné
                    ?>
                <div style="display: flex; justify-content: space-around; padding: 30px;">
                    <div class="bloc_compet">
                    <div class="msg_resultat_ins">
                            <p><?= (count($edition['instances']) == 0 ? "Pas d'édition enregistré.": count($edition['instances'])." éditions enregistrées") ?></p>
                    </div>
                    <?php 
                    if(count($edition['instances']) != 0)
                    { ?>
                        <div class="res_ins">
                            <table class="table_resultat">
                                <thead>
                                    <tr>
                                    <?php
                                            foreach($edition['schema'] as $att) {  // pour parcourir les attributs
                                                echo '<th>';
                                                    echo att_to_nom($att['nom']);
                                                echo '</th>';
                                                
                                            }
                                            echo '<th>Cliquez pour modifier</th>';
                                        ?>	
                                        </tr>	
                                        </thead>
                                    <tbody>

                                    <?php
                                    
                                        foreach($edition['instances'] as $row) {  // pour parcourir les n-uplets
                                    
                                        echo '<tr>';
                                        $keys = array_keys($row);
                                        array_push($row,"Modifier");
                                        foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                            if($valeur == 'Modifier')
                                            {
                                                echo '<td>'. '<a href="./index.php?page=federation_compet&idF='.$_GET['idF'].'&idCom='.$_GET['idCom'].'&annee='.$row['année'].'&ville='.$row['ville_organisatrice'].'&idS='.$row['idS'].'&modifier=#">Modifier</a>' . '</td>';
                                            }
                                            else
                                            {
                                                echo '<td>'. $valeur . '</td>';
                                            }
                                        }
                                        echo '</tr>';
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
                <div class="bloc_compet">
                    <h2>Voici les options disponibles : </h2>
                    <h2>
                        <ol class="links">
                            <li><a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>&ajouter=edition">Ajouter une édition</a></li></br>
                            <li><a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>&modifier=#">Modifier les informations de la compétition</a></li> </br> 
                            <li><a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>&inscrire=groupe">Inscrire un groupe</a></li></br>
                            <li><a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>&inscrire=couple">Inscrire un couple</a></li></br>
                            <li><a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>&affecter=couple">Affecter un rang (couple)</a></li></br>
                            <li><a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>&affecter=groupe">Affecter un rang (groupe)</a></li></br>
                        </ol>
                    </h2>
                </div>
            </div>
            <?php }else{
            {
                if(isset($_GET['affecter'])) { ?>
                    <?php if($_GET['affecter'] == "couple") { ?>
                        <h2> Affectation d'un rang (couple) </h2>
                        <?php if(!isset($_POST['selecC'])) { ?>
                        <form action="#" class="form_accueil" method="post">
                            <h1>Veuillez selectionner un couple de danseurs</h1>
                            <select name="couple" id="couple">
                                <?php foreach($couples['instances'] as $row){?>
                                    <option value="<?= $row['Licence1'] ?> <?= $row['Licence2'] ?>"><?= $row['nom1'] ?> et <?= $row['nom2'] ?></option>
                                <?php }?>
                            </select>
                            <input type="submit" value="Choisir" name="selecC">
                        </form>
                        <?php }else {?>
                        <form action="#" class="form_accueil" method="post">
                            <input type="hidden" value="<?= $_POST['couple'] ?>" name="couple">
                            <h1> Selectionner une édition puis le rang à affecter</h1>
                            <label for="annee"> Edition : </label>
                            <select name="annee" id="annee">
                                <?php foreach($edition['instances'] as $row){?>
                                    <option value="<?= $row['année'] ?>"><?= $row['année'] ?></option>
                                <?php }?>
                            </select>
                            <label for="rang"> Rang : </label><input type="text" value="<?= $row['rf'] ?>" name="rang">
                            <input type="submit" value="Affecter" name="affecterC">
                        </form>
                        <?php } ?>
                        <div class="msg_resultat_ins">
                            <h1><?=$message_err?></h1>
                        </div> 
                        <div class = "retour">
                            <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>"> Retour </a>
                        </div>
                    <?php } ?> 
                    
                    <?php if($_GET['affecter'] == "groupe") { ?>
                        <h2> Affectation d'un rang (couple) </h2>
                        <?php if(!isset($_POST['selecG'])) { ?>
                        <form action="#" class="form_accueil" method="post">
                            <h1>Veuillez selectionner un couple de danseurs</h1>
                            <select name="groupe" id="groupe">
                                <?php foreach($groupes['instances'] as $row){?>
                                    <option value="<?= $row['idGroupe'] ?>"><?= $row['nom'] ?></option>
                                <?php }?>
                            </select>
                            <input type="submit" value="Choisir" name="selecG">
                        </form>
                        <?php }else {?>
                        <form action="#" class="form_accueil" method="post">
                            <input type="hidden" value="<?= $_POST['groupe'] ?>" name="groupe">
                            <h1> Selectionner une édition puis le rang à affecter</h1>
                            <label for="annee"> Edition : </label>
                            <select name="annee" id="annee">
                                <?php foreach($edition['instances'] as $row){?>
                                    <option value="<?= $row['année'] ?>"><?= $row['année'] ?></option>
                                <?php }?>
                            </select>
                            <label for="rang"> Rang : </label><input type="text" value="<?= $row['rf'] ?>" name="rang">
                            <input type="submit" value="Affecter" name="affecterG">
                        </form>
                        <?php } ?>
                        <div class="msg_resultat_ins">
                            <h1><?=$message_err?></h1>
                        </div> 
                        <div class = "retour">
                            <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>"> Retour </a>
                        </div>
                    <?php } ?>

                <?php } ?>

                <?php if(isset($_GET['inscrire']))
                {
                    if($_GET['inscrire'] == "couple") { // INSCRIPTION D'UN COUPLE À UNE ÉDITION DE COMPÉTITION?>
                        <h2> Inscription d'un couple </h2>
                        <form action="#" class="form_accueil" method="post">
                            <h1>Veuillez selectionner 2 danseurs (différents) </h1>
                            </br>
                            <select name="danseur1" id="danseur1">
                                <?php foreach($adherent['instances'] as $row){?>
                                    <option value="<?= $row['Licence'] ?>"><?= $row['Nom'] ?> <?= $row['Prénom'] ?></option>
                                <?php }?>
                            </select>
                            <select name="danseur2" id="danseur1">
                                <?php foreach($adherent['instances'] as $row){?>
                                    <option value="<?= $row['Licence'] ?>"><?= $row['Nom'] ?> <?= $row['Prénom'] ?></option>
                                <?php }?>
                            </select>
                            </br>
                            <h1> Veuillez choisir une édition de la compétition </h1>
                            </br>
                            <select name="annee" id="annee">
                                <?php foreach($edition['instances'] as $row){?>
                                    <option value="<?= $row['année'] ?>"><?= $row['année'] ?> <?= $row['ville_organisatrice'] ?></option>
                                <?php }?>
                            </select>
                            </br>
                            </br>
                            <input type="submit" value="Inscrire" name="inscrireC">
                        </form>
                        <div class="msg_resultat_ins">
                            <h1><?=$message_err?></h1>
                        </div> 
                        <div class = "retour">
                            <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>"> Retour </a>
                        </div>
                    <?php } ?>
                

                    <?php if($_GET['inscrire'] == "groupe") { ?>
                        <h2> Inscription d'un groupe </h2>
                        <form action="#" class="form_accueil" method="post">
                            <h1>Veuillez selectionner un groupe de danseurs  </h1>
                            </br>
                            <select name="groupe" id="groupe">
                                <?php foreach($groupes['instances'] as $row){?>
                                    <option value="<?= $row['idGroupe'] ?>"><?= $row['nom'] ?></option>
                                <?php }?>
                            </select>
                            </br>
                            <h1> Veuillez choisir une édition de la compétition </h1>
                            </br>
                            <select name="annee" id="annee">
                                <?php foreach($edition['instances'] as $row){?>
                                    <option value="<?= $row['année'] ?>"><?= $row['année'] ?> <?= $row['ville_organisatrice'] ?></option>
                                <?php }?>
                            </select>
                            </br>
                            </br>
                            <input type="submit" value="Inscrire" name="inscrireG">
                        </form>
                        <div class="msg_resultat_ins">
                            <h1><?=$message_err?></h1>
                        </div> 
                        <div class = "retour">
                            <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>"> Retour </a>
                        </div>
                    <?php } ?>


            <?php } ?>


            <?php   if(isset($_GET['modifier'])) // Bloc des modifications 
                {
                    if(!isset($_GET['annee'])) // Modif de compétition
                {?>
                <div class="modif">
                    <h1>Modification de la compétition : </h1>
                </br>
                    <form action="#" class="form_accueil" method="post">
                        <select name="niveau" id="niveau">
                            <option value="National">National</option>
                            <option value="Départemental">Départemental</option>
                            <option value="Régional">Régional</option>
                        </select>
                        <input type="text" name="libelle" placeholder="Entrez le libellé" value=" <?= $competition['instances'][0]['libelléCompet'] ?>"/>
                        <input type="submit" value="Modifier" name="ModifierCom">
                    </form>
                    <div class="msg_resultat_ins">
                        <h1><?=$message_err?></h1>
                    </div> 
                    <div class = "retour">
                        <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>"> Retour </a>
                    </div>
                </div>

            <?php }else{ ?>
                <div class="modif">
                    <h1>Modification de l'édition : </h1>
                    </br>

                    <form action="#" class="form_accueil" method="post">
                        <label for="annee">Ville organisatrice </label><input type="text" name="ville" placeholder="Entrez la ville" value = "<?= $_GET['ville'] ?>"/> </br></br></br>
                        <h1>Modification de la structure</h1></br>
                        <label for="annee">Nom de la structure </label><input type="text" name="nomStruct" placeholder="Nom de la structure" value = "<?= $structure['nom'] ?>"/>
                        <label for="annee">Type de la structure </label><input type="text" name="typeStruct" placeholder="Type de la structure" value = "<?= $structure['type'] ?>"/></br></br>
                        <input type="submit" value="Modifier" name="ModifierEd">
                </div>
                <div class="msg_resultat_ins">
                    <h1><?=$message_err?></h1>
                </div>                
                <div class = "retour">
                    <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>"> Retour </a>
                </div>
            <?php  }


            }
            if(isset($_GET['ajouter'])) { 
                if($_GET['ajouter'] == "edition"){ // Bloc d'ajout d'une édition ?>
            <div class="ajout">
                <h1> Ajout d'une édition </h1>
                </br>

                <form action="#" class="form_accueil" method="post">
                    <label for="annee">Année </label>
                    <select name="annee" id="annee">
                        <?php for($i = 2050; $i >= 2023; $i--){ 
                            if(!in_array($i,$annee_prise)){  ?>
                            <option value="<?= $i ?>"> <?= $i ?> </option>
                        <?php }} ?>
                    </select>
                    <label for="annee">Ville organisatrice </label><input type="text" name="ville" placeholder="Entrez la ville"/>
                    <input type="submit" value="Ajouter" name="Ajouter">
                </form>
            </div>
            <div class="msg_resultat_ins">
                <h1><?=$message_err?></h1>
            </div> 
            <div class = "retour">
                <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>"> Retour </a>
            </div>
            <?php 
                } ?>
            <?php
                }
                }
                }
                }
                
                if(isset($_GET['ajouter'])){
                    if($_GET['ajouter'] == "compet"){ ?>
                <div class="ajout">
                    <h1>Ajout d'une nouvelle compétition : </h1>
                </br>
                    <form action="#" class="form_accueil" method="post">
                        <input type="text" name="code" placeholder="Entrez le code "/>
                        <select name="niveau" id="niveau">
                            <option value="National">National</option>
                            <option value="Départemental">Départemental</option>
                            <option value="Régional">Régional</option>
                        </select>
                        <input type="text" name="libelle" placeholder="Entrez le libellé" />
                        <input type="submit" value="Ajouter" name="Ajouter">
                    </form>
                    <div class="msg_resultat_ins">
                        <h1><?=$message_err?></h1>
                    </div> 
                    <div class = "retour">
                        <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>"> Retour </a>
                    </div>
                </div>
                <?php } ?>
        <?php } ?>
        <?php }
        ?>
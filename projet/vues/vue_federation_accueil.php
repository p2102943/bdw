<?php
if($message == "Erreur !")
{ ?>
    <div class="accueil">  
    <div class="titre_page">
        <h1><?= $message ?> </h1>
    </div>

<?php
} else {
    if( !isset($_GET['Valider']))
    {
?>


<div class="accueil">  
    <div class="titre_page">
        <h1><?= $message ?> </h1>
    </div>
    <form action="#" class="form_accueil" method="get">
        <input name="page" type="hidden" value="federation_accueil">
        <select name="idF" id="idF">
            <?php foreach($responsables['instances'] as $row){?>
                <option value="1"><?= $row['nom'] ?> <?= $row['prenom'] ?></option>
            <?php }?>
        </select>
        <input type="submit" value="Valider" name="Valider">
    </form>
</div>

<?php }
    else{?>
        <div class="federation">
            <div class="info_accueil">
                <h2> <?= $nomFede ?>, <?= $sigle ?> </h2>
                <h2>Adresse : 
                    <?php
                        foreach($info_fede['instances'] as $row){
                            foreach($row as $att){
                                if($att != $nomFede && $att != $sigle)
                                {echo $att." ";}
                    }}?>
                </h2>
            </div>
            <div class="titre_page">
                <h1>Tableau de bord de fédération</h1>
            </div>
            <div style="padding: 20px;">
                <h2>Accéder à :</h2> 
                <ul class="links">
                    <li><a href="index.php?page=federation_compet&idF=<?= $_GET['idF']?>">Gestion des fédérations</a></li>
                    <li><a href="index.php?page=federation_compet&idF=<?= $_GET['idF']?>">Gestion des comités</a></li>
                    <li><a href="index.php?page=federation_membre&idF=<?= $_GET['idF']?>">Gestion des membres</a></li>
                    <li><a href="index.php?page=federation_compet&idF=<?= $_GET['idF']?>">Gestion des compétitions</a></li>
                </ul>
            </div>
            </br>

            <div class="stats_accueil">
                <div class="bloc_stats">
                    <p>Voici le nombre de membres dans cette fédération :</p>
                    <h1 style="color: #cb88ff;"><?=count($responsables['instances'])?></h1>
                </div>
                <div class="bloc_stats"> <!-- Bloc de statisqtiques des comités -->
                    <p>Voici le nombre de comité dans cette fédération :</p>
                    <h1 style="color: #cb88ff;"><?=count($comite_fed['instances'])?></h1>
                </div>
                <div class="bloc_stats"><!-- Bloc de statisqtiques des compétitions  -->
                    <p><?= $message_comp ?></p>
                    <?php
                    if(count($competition_fed['instances']) != 0)
                    {
                    ?>
                        <table class="table_resultat">
                        <thead>
                            <tr>
                            <?php
                                //var_dump($resultats);
                                foreach($competition_fed['schema'] as $att) {  // pour parcourir les attributs
                        
                                    echo '<th>';
                                        echo att_to_nom($att['nom']);
                                    echo '</th>';
                        
                                }
                            ?>	
                            </tr>	
                            </thead>
                        <tbody>

                        <?php
                        
                            foreach($competition_fed['instances'] as $row) {  // pour parcourir les n-uplets
                        
                            echo '<tr>';
                            foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                    echo '<td>'. $valeur . '</td>';
                            }
                            echo '</tr>';
                        }
                    ?>
                    </tbody>
                </table>

                    <?php } ?>
                </div>
                <div class="bloc_stats">
                    <p>Voici le nombre d'adhérents participants à une compétition au total</p>
                    <h1 style="color: #cb88ff;"><?= $nbAdCompet ?></h1>
                </div>
            </div>
        </div>



<?php }} ?>
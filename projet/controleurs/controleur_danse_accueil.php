<?php

    $message = "Veuillez selectionner votre nom : ";
    $responsables = get_responsables();
    if($responsables == null)
    {
        $message = "Il n'y a pas de responsables pour l'instant...";
    }

    if(isset($_GET['Valider']))
    {
        //var_dump($_POST['idEcole']);
        $idEcole = mysqli_real_escape_string($connexion, trim($_GET['idEcole']));
        $nbAd = get_nbAd_annee(2022,$idEcole);
        $nbAdCompet = get_nbAd_compet();
        $info_ecole = get_infos_ecole($idEcole);
        $nomEcole = $info_ecole['instances'][0]['nomEcole'];


        $employe = get_employe($idEcole);
        if($employe['instances'] ==  null)
        {
            $message_emp = "Il n'y a pas d'employé dans cette école.";
        }
        else
        {
            $message_emp = count($employe['instances']).(count($employe['instances']) >1? " employés : ":" employé : ");
        }

        $cours = get_cours_ecole($idEcole);
        if($cours['instances'] == null)
        {
            $message_cours = "Il n'y a pas de cours enregisté dans cette école.";
        }
        else
        {
            $message_cours = count($cours['instances'])." cours proposés par l'école : ";
        }
    }


?>
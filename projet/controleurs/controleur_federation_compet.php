<?php
    $message = "";
    $message_err = "";
    $reussi = false;


    if(isset($_GET['supprimer'])){
        if(isset($_GET['annee'])){
            mysqli_query($connexion,'DELETE from Edition WHERE codeCompet = "'.$_GET['idCom'].'" AND année = '.$_GET['annee'].';');
        }   
        else
        {
            mysqli_query($connexion,'DELETE from Edition WHERE codeCompet = "'.$_GET['code'].'";');
            mysqli_query($connexion,'DELETE from Compétition WHERE codeCompet = "'.$_GET['code'].'";');
        }
    }

    $competition = get_instances_by_requete("SELECT codeCompet , niveauCompet , libelléCompet FROM Compétition where idF = ".$_GET['idF'].";");
    if(count($competition['instances']) == 0)
    {
        $message = "Il n'y a pas de compétitions dans cette fédération.";
    }
    else
    {
        $message = (count(($competition['instances'])) > 1?" Compétitions : ":" Compétition : ");
    }

    if(isset($_GET['idCom']))
    {
        $annee_ed = get_instances_by_requete("SELECT année FROM Edition WHERE codeCompet = '".$_GET['idCom']."';");
        $annee_prise = array();
        foreach($annee_ed as $row)
        {
            foreach($row as $a)
            {
                if(isset($a['année']))
                    array_push($annee_prise,(int) $a['année']);
            }
        }
    }

    if(isset($_POST["ModifierCom"]))
    {
        $niveau = mysqli_real_escape_string($connexion, trim($_POST['niveau']));
        $libelle = mysqli_real_escape_string($connexion, trim($_POST['libelle']));
        $message_err = "";
        if($libelle == "")
        {
            $message_err = "Veuillez remplir tous les champs.";
        }
        else
        {
            $reussi = mysqli_query($connexion,'UPDATE Compétition SET niveauCompet = "'.$niveau.'", libelléCompet = "'.$libelle.'" WHERE idF = '.$_GET['idF'].' AND codeCompet = "'.$_GET['idCom'].'";');
            $message_err = $reussi? "Modifié avec succès ! ": "Erreur lors de la modification.";
        }
    }

    if(isset($_GET['modifier']) && isset($_GET['idS'])) 
    {
        $idS = mysqli_real_escape_string($connexion, trim($_GET['idS']));
        $structure = get_nom_type_struct($idS);
    }

    if(isset($_POST['ModifierEd']))
{
        $annee = mysqli_real_escape_string($connexion, trim($_GET['annee']));
        $ville = mysqli_real_escape_string($connexion, trim($_POST['ville']));
        $nomStruct = mysqli_real_escape_string($connexion, trim($_POST['nomStruct']));
        $typeStruct = mysqli_real_escape_string($connexion, trim($_POST['typeStruct']));
        $idS = mysqli_real_escape_string($connexion, trim($_GET['idS']));
        $idCom = mysqli_real_escape_string($connexion, trim($_GET['idCom']));


        $message_err = "";

        if($ville == "" || $nomStruct == "" || $typeStruct == "")
        {
            $message_err = "Veuillez remplir tous les champs.";
        }
        else
        {
            modifier_structure($idS,$nomStruct,$typeStruct);
            $reussi = modifier_edition($idCom,$annee,$ville);
            $message_err = $reussi? "Modifié avec succès ! ": "Erreur lors de la modification.";
        }
    }


    if(isset($_POST["Ajouter"])){
        if(isset($_GET['idCom']))
        {
            $annee = mysqli_real_escape_string($connexion, trim($_POST['annee']));
            $ville = mysqli_real_escape_string($connexion, trim($_POST['ville']));
            $message_err = "";
            $reussi = false;
            if($ville == "")
            {
                $message_err = "Veuillez remplir tous les champs.";
            }
            else
            {
                $reussi = mysqli_query($connexion,'INSERT INTO Edition VALUES (  "'.$_GET['idCom'].'"   ,'.$annee.'  ,"'.$ville.'", NULL )');
                $message_err = $reussi? "Ajouté avec succès ! ": "Erreur lors de l'ajout.";
            }
        }else
        {
            $niveau = mysqli_real_escape_string($connexion, trim($_POST['niveau']));
            $libelle = mysqli_real_escape_string($connexion, trim($_POST['libelle']));
            $code = mysqli_real_escape_string($connexion, trim($_POST['code']));
            $code = strtoupper($code);
            $requete = "INSERT INTO Compétition (codeCompet, niveauCompet, libelléCompet, idF) VALUES (  '".$code."'   ,'".$niveau."'  ,'".$libelle."',".$_GET['idF']." );";
            $message_err = "";
            if($libelle == "" || $code = "")
            {
                $message_err = "Veuillez remplir tous les champs.";
            }
            else
            {
                $reussi = mysqli_query($connexion,$requete);
                $message_err = $reussi? "Ajouté avec succès ! ": "Erreur lors de l'ajout.";
            }
        }
    }

    if(isset($_POST['boutonRechercher']))
    {
        if(!empty(trim($_POST['mots_cles'])))
        {
            $mots_cle = explode(" ", mysqli_real_escape_string($connexion, trim($_POST['mots_cles'])));
            $attribut = mysqli_real_escape_string($connexion, trim($_POST['attributRecherche']));
            for($i = 0; $i < count($mots_cle); $i++)
            {
                $kw[$i] = $attribut." LIKE '%".$mots_cle[$i]."%'";
            }
            $competition = get_instances_by_requete("SELECT DISTINCT codeCompet , niveauCompet , libelléCompet as libellé FROM Compétition where idF = ".$_GET['idF']." AND ".implode(" or ",$kw));
            if(count($competition['instances']) == 0)
            {
                $message = "Aucun résultat trouvé.";
            }
            else
            {
                $message = count($competition['instances']).(count(($competition['instances'])) > 1?" Résultats trouvés pour les adhérants : ":" Resultat trouvé pour les adhérants : ");
            }
        }
    }

    if(isset($_GET['idCom']))
    {
        $message = "Voici la compétition selectionnée.";
        $competition = get_instances_by_requete("SELECT codeCompet , niveauCompet , libelléCompet FROM Compétition where idF = ".$_GET['idF']." AND codeCompet = '".$_GET['idCom']."';");
        $edition = get_instances_by_requete("SELECT année, ville_organisatrice, idS FROM Edition e JOIN Compétition c USING (codeCompet) where c.idF = ".$_GET['idF']." AND e.codeCompet = '".$_GET['idCom']."';");
        if(isset($_GET['inscrire'])){
            $type = $_GET['inscrire'] ;
            if($type == "couple")
            {
                $adherent = get_infos_ad();
            }
    
            if($type == "groupe")
            {
                $groupes = get_groupes();
            }
        }
    }

    if(isset($_POST['inscrireC']))
    {
        $danseur1 = mysqli_real_escape_string($connexion, trim($_POST['danseur1']));
        $danseur2 = mysqli_real_escape_string($connexion, trim($_POST['danseur2']));
        $annee = mysqli_real_escape_string($connexion, trim($_POST['annee']));
        $idCom = mysqli_real_escape_string($connexion, trim($_GET['idCom']));

        if($danseur1 == $danseur2)
        {
            $message_err = "Veuillez selectionner 2 danseurs distincts.";
        }
        else{
            $reussi = ajouter_couple($danseur1,$danseur2) && inscrire_couple($danseur1,$danseur2,$idCom,$annee);
            if($reussi)
            {
                $message_err = "Couple inscrits.";
            }
            else
            {
                $message_err = "Erreur lors de l'inscription du couple.";
            }
        }
        
    }


    if(isset($_POST['inscrireG']))
    {
        $groupe = mysqli_real_escape_string($connexion, trim($_POST['groupe']));
        $annee = mysqli_real_escape_string($connexion, trim($_POST['annee']));
        $idCom = mysqli_real_escape_string($connexion, trim($_GET['idCom']));

        $reussi = inscrire_groupe($groupe,$idCom,$annee);
        if($reussi)
        {
            $message_err = "Groupe inscrit.";
        }
        else
        {
            $message_err = "Erreur lors de l'inscription du groupe.";
        }
    }
        



    if(isset($_GET['affecter']))
    {
        if($_GET['affecter'] == "couple")
        {
            $couples = get_couples($_GET['idCom']);
        }


        if($_GET['affecter'] == "groupe")
        {
            $groupes = get_couples_inscrits($_GET['idCom']);
        }

    }

    if(isset($_POST['selecC']))
    {
        $couple = mysqli_real_escape_string($connexion, trim($_POST['couple']));
        $edition = get_edition_from_couple($couple,$_GET['idCom']);
    }

    if(isset($_POST['affecterC']))
    {
        $couple = mysqli_real_escape_string($connexion, trim($_POST['couple']));
        $annee = mysqli_real_escape_string($connexion, trim($_POST['annee']));
        $rang = mysqli_real_escape_string($connexion, trim($_POST['rang']));

        if($rang != "")
        {
            if(modifier_rang_couple($couple,$_GET['idCom'],$annee,$rang))
            {
                $message_err = "Rang affecté au couple.";
            }
            else
            {
                $message_err = "Erreur lors de l'affectation.";
            }
        }
        else
        {
            $message_err = "Veuillez selectionner un rang ! ";
        }
    }

    if(isset($_POST['selecG']))
    {
        $groupe = mysqli_real_escape_string($connexion, trim($_POST['groupe']));
        $edition = get_edition_from_groupe($groupe,$_GET['idCom']);
    }

    if(isset($_POST['affecterG']))
    {
        $groupe = mysqli_real_escape_string($connexion, trim($_POST['groupe']));
        $annee = mysqli_real_escape_string($connexion, trim($_POST['annee']));
        $rang = mysqli_real_escape_string($connexion, trim($_POST['rang']));

        if($rang != "")
        {

            if(modifier_rang_groupe($groupe,$_GET['idCom'],$annee,$rang))
            {
                $message_err = "Rang affecté au couple.";
            }
            else
            {
                $message_err = "Erreur lors de l'affectation.";
            }
        }
        else
        {
            $message_err = "Veuillez selectionner un rang ! ";
        }
    }
    
   



?>
<?php
    $message = "";

    $adherant = get_ad_ecole($_GET['idEcole']);

    if(count($adherant['instances']) == 0)
    {
        $message = "Il n'y a pas d'adhérant inscrits dans cette école.";
    }
    else
    {
        $message = (count(($adherant['instances'])) > 1?" Adhérants inscrits : ":" Adhérant inscrit : ");
    }

    if(isset($_GET['idCours']) && isset($_GET['idEcole']))
    {
        ajouter_ad_cours($_GET['idEcole'],$_GET['identifiant'],$_GET['idCours']);
    }

    if(isset($_GET['supprimer']))
    {
        supprimer_ad_cours($_GET['idEcole'],$_GET['idCours'],$_GET['identifiant']);
    }

    if(isset($_POST['boutonRechercher']))
    {
        if(!empty(trim($_POST['mots_cles'])))
        {
            $mots_cle = explode(" ", mysqli_real_escape_string($connexion, trim($_POST['mots_cles'])));
            $attribut = mysqli_real_escape_string($connexion, trim($_POST['attributRecherche']));
            for($i = 0; $i < count($mots_cle); $i++)
            {
                $kw[$i] = $attribut." LIKE '%".$mots_cle[$i]."%'";
            }
            $adherant = get_ad_filtre($_GET['idEcole'],$kw);
            if(count($adherant['instances']) == 0)
            {
                $message = "Aucun résultat trouvé.";
            }
            else
            {
                $message = count($adherant['instances']).(count(($adherant['instances'])) > 1?" Résultats trouvés pour les adhérants : ":" Resultat trouvé pour les adhérants : ");
            }
        }
    }

    if(isset($_GET['identifiant']))
    {
        $message = "Voici l'adhérant selectionné.";
        $cours_ad = get_instances_by_requete("SELECT DISTINCT c.idCours, c.libellé, c.idE, c.categorie_age FROM Cours c JOIN est_inscrit ass USING(idCours,idE) JOIN Adhérant a USING(numLicence) WHERE a.numLicence = ".$_GET['identifiant']." AND c.idE = ".$_GET['idEcole'].";");
        $cours = get_instances_by_requete("SELECT DISTINCT * FROM `Cours` WHERE idE =".$_GET['idEcole'].";");
        $adherant = get_instances_by_requete("SELECT DISTINCT * FROM Adhérant WHERE numLicence =".$_GET['identifiant'].";");
        if(count($cours['instances']) == 0)
        {
            $message_cours = "Aucun cours disponibles.";
        }
        else
        {
            $message_cours = "Voici les ".count($cours['instances'])." cours disponibles : ";
        }
        if($cours_ad['instances'] == null)
        {
            $message_ad = "Cet adhérant n'assiste à aucun cours dans cette école.";
        }
        else
        {
            $message_ad = "Cet adhérant assiste à ".count($cours_ad['instances'])." cours.";
        }
    }

    
?>
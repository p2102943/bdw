<?php

    $controleur = 'controleur_accueil.php';
    $vue = 'vue_accueil.php';
    /* Affectation du controleur et de la vue souhaités */
    if(isset($_GET['page'])) {
        // récupération du paramètre 'page' dans l'URL
        $nomPage = $_GET['page'];
        $vue = 'vue_'.$nomPage.'.php';
        // construction des noms de con,trôleur et de vue
        $controleur = 'controleur_'.$nomPage.'.php';
    }
    /* Inclusion du contrôleur et de la vue courante */
    require('controleurs/' . $controleur);
    require('vues/'.$vue);

?>
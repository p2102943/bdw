<?php

    $message = "";
    $message_err = "";
    $message_supp = "";

    if(isset($_GET['ModifierComite']))
    {

        $nom = mysqli_real_escape_string($connexion, trim($_GET['nom']));
        $prenom = mysqli_real_escape_string($connexion, trim($_GET['prenom']));
        $fonction = mysqli_real_escape_string($connexion, trim($_GET['fonction']));
        $jour = mysqli_real_escape_string($connexion, trim($_GET['jour']));
        $mois = mysqli_real_escape_string($connexion, trim($_GET['mois']));
        $annee = mysqli_real_escape_string($connexion, trim($_GET['annee']));

        $date_act = $jour."/".$mois."/".$annee;
        if($nom == "" || $prenom == "")
        {
            $message_err = "Veuillez remplir tous les champs";
        }
        else
        {   
            $reussi = mysqli_query($connexion,'UPDATE MembreComité SET nom = "'.$nom.'", prenom = "'.$prenom.'", dateNaissance = "'.$date_act.'", fonction = "'.$fonction.'" WHERE idMem = '.$_GET['idMem'].' AND idC = "'.$_GET['comite'].'";');
            $message_err = $reussi? "Modifié avec succès ! ": "Erreur lors de la modification.";
        }
    }

    if(isset($_POST['ajouterMembre']))
    {
        if($_GET['ajouter'] == "membre")
        {
            $nom = mysqli_real_escape_string($connexion, trim($_POST['nom']));
            $prenom = mysqli_real_escape_string($connexion, trim($_POST['prenom']));
            $fonction = mysqli_real_escape_string($connexion, trim($_POST['fonction']));
            $jour = mysqli_real_escape_string($connexion, trim($_POST['jour']));
            $mois = mysqli_real_escape_string($connexion, trim($_POST['mois']));
            $annee = mysqli_real_escape_string($connexion, trim($_POST['annee']));


            $date = $jour."/".$mois."/".$annee;
            $message_err = "";
            $reussi = false;
            if($nom == "" || $prenom =="" || $fonction == "")
            {
                $message_err = "Veuillez remplir tous les champs.";
            }
            else
            {
                $reussi = mysqli_query($connexion,'INSERT INTO MembreComité (nom,prenom,dateNaissance,fonction,idC) VALUES ("'.$nom.'","'.$prenom.'","'.$date.'","'.$fonction.'",'.$_GET['comite'].');');
                $message_err = $reussi? "Ajouté avec succès ! ": "Erreur lors de l'ajout.";
            }
        }
    }

    if(isset($_GET['supprimer']))
    {
        if($_GET['supprimer'] == "membre"){
            $reussi = mysqli_query($connexion,'DELETE FROM MembreComité WHERE idMem = '.$_GET['idSupp'].' AND idC = '.$_GET['comite'].';');
            $message_supp = $reussi?"Suppression réussi  !":"Echec de la suppresion ! ";
        }
    }



    $comite = get_instances_by_requete("SELECT idC, idF, nomComité, niveau FROM Comité;");
    if(count($comite['instances']) == 0)
    {
        $message = "Cette fédération ne contient pas de comité.";
    }
    else
    {
        if(!isset($_GET['comite'])) $message = "Veuillez selectionner un comité.";
        else $message = "Voici les membres du comité choisi.";
    }

    if(isset($_GET['Selec']))
    {
        $comite_selec = get_instances_by_requete("SELECT idC, idF, nomComité, niveau FROM Comité where idC = ".$_GET['comite']."; ");
        $membre = get_instances_by_requete("SELECT m.idMem, m.nom, m.prenom as prénom , m.dateNaissance, fonction FROM MembreComité m WHERE m.idC = ".$_GET['comite'].";");
        $message_m = "";
        if($membre == null)
        {
            $message_m = "Erreur ! ";
        }
        else
        {
            if(count($membre['instances']) == 0)
            {
                $message_m = "Ce comité n'a pas de membre.";
            }
            else{
                $message_m = count($membre['instances']) > 1? count($membre['instances'])." Membres pour le comité : ".$comite_selec['instances'][0]['nomComité'] : count($membre['instances'])." Membre pour le comité : ".$comite_selec['instances'][0]['nomComité'];
            }
        }
    }

    if(isset($_GET['visualiser']))
    {
        if(isset($_GET['idMem']))
        {
            $membre = get_instances_by_requete("SELECT m.idMem, m.nom, m.prenom as prénom, m.dateNaissance, m.idC as comité, fonction FROM MembreComité m WHERE m.idC = ".$_GET['comite']." AND m.idMem = ".$_GET['idMem'].";");
            $date_prec = explode('/',$membre['instances'][0]['dateNaissance']);
            $message_m = "Voici le membre selectionné.";
        }
    }

    

?>
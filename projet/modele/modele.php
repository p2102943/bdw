<?php 

/*
Structure de données permettant de manipuler une base de données :
- Gestion de la connexion
----> Connexion et déconnexion à la base
- Accès au dictionnaire
----> Liste des tables et statistiques
- Informations (structure et contenu) d'une table
----> Schéma et instances d'une table
- Traitement de requêtes
----> Schéma et instances résultant d'une requête de sélection
*/



////////////////////////////////////////////////////////////////////////
///////    Gestion de la connxeion   ///////////////////////////////////
////////////////////////////////////////////////////////////////////////

/**
 * Initialise la connexion à la base de données courante (spécifiée selon constante 
	globale SERVEUR, UTILISATEUR, MOTDEPASSE, BDD)			
 */
function open_connection_DB() {
	global $connexion;

	$connexion = mysqli_connect(SERVEUR, UTILISATEUR, MOTDEPASSE, BDD);
	if (mysqli_connect_errno()) {
	    printf("Échec de la connexion : %s\n", mysqli_connect_error());
	    exit();
	}
}

/**
 *  	Ferme la connexion courante
 * */
function close_connection_DB() {
	global $connexion;

	mysqli_close($connexion);
}


////////////////////////////////////////////////////////////////////////
///////   Accès au dictionnaire       ///////////////////////////////////
////////////////////////////////////////////////////////////////////////

/**
 *  Retourne la liste des tables définies dans la base de données courantes
 * */
function get_tables() {
	global $connexion;

	$requete = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA LIKE '". BDD ."'";

	$res = mysqli_query($connexion, $requete);
	$instances = mysqli_fetch_all($res, MYSQLI_ASSOC);
	return $instances;
}


/**
 *  Retourne les statistiques sur la base de données courante
 * */
function get_statistiques() {
	global $connexion;
	$requete = "SELECT table_name, table_rows FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA LIKE '". BDD ."'";
	$res = mysqli_query($connexion, $requete);
	$instances = mysqli_fetch_all($res, MYSQLI_ASSOC);
	return $instances;
}

////////////////////////////////////////////////////////////////////////
///////    Informations (structure et contenu) d'une table    //////////
////////////////////////////////////////////////////////////////////////

/**
 *  Retourne le détail des infos sur une table
 * */
function get_infos( $typeVue, $nomTable ) {
	global $connexion;

	switch ( $typeVue) {
		case 'schema': return get_infos_schema( $nomTable ); break;
		case 'data': return get_infos_instances( $nomTable ); break;
		default: return null; 
	}
}

/**
 * Retourne le détail sur le schéma de la table
*/
function get_infos_schema( $nomTable ) {
	global $connexion;

	// récupération des informations sur la table (schema + instance)
	$requete = "SELECT * FROM $nomTable";
	$res = mysqli_query($connexion, $requete);

	// construction du schéma qui sera composé du nom de l'attribut et de son type	
	$schema = array( array( 'nom' => 'nom_attribut' ), array( 'nom' => 'type_attribut' ) , array('nom' => 'clé')) ;

	// récupération des valeurs associées au nom et au type des attributs
	$metadonnees = mysqli_fetch_fields($res);

	$infos_att = array();
	foreach( $metadonnees as $att ){
		//var_dump($att);

 		$is_in_pk = ($att->flags & MYSQLI_PRI_KEY_FLAG)?'PK':'';
 		$type = convertir_type($att->{'type'});

		array_push( $infos_att , array( 'nom' => $att->{'name'}, 'type' => $type , 'cle' => $is_in_pk) );	
	}

	return array('schema'=> $schema , 'instances'=> $infos_att);

}

/**
 * Retourne les instances de la table
*/
function get_infos_instances( $nomTable ) {
	global $connexion;

	// récupération des informations sur la table (schema + instance)
	$requete = "SELECT * FROM $nomTable";  
 	$res = mysqli_query($connexion, $requete);  

 	// extraction des informations sur le schéma à partir du résultat précédent
	$infos_atts = mysqli_fetch_fields($res); 

	// filtrage des information du schéma pour ne garder que le nom de l'attribut
	$schema = array();
	foreach( $infos_atts as $att ){
		array_push( $schema , array( 'nom' => $att->{'name'} ) ); // syntaxe objet permettant de récupérer la propriété 'name' du de l'objet descriptif de l'attribut courant
	}

	// récupération des données (instances) de la table
	$instances = mysqli_fetch_all($res, MYSQLI_ASSOC);

	// renvoi d'un tableau contenant les informations sur le schéma (nom d'attribut) et les n-uplets
	return array('schema'=> $schema , 'instances'=> $instances);

}


function convertir_type( $code ){
	switch( $code ){
		case 1 : return 'BOOL/TINYINT';
		case 2 : return 'SMALLINT';
		case 3 : return 'INTEGER';
		case 4 : return 'FLOAT';
		case 5 : return 'DOUBLE';
		case 7 : return 'TIMESTAMP';
		case 8 : return 'BIGINT/SERIAL';
		case 9 : return 'MEDIUMINT';
		case 10 : return 'DATE';
		case 11 : return 'TIME';
		case 12 : return 'DATETIME';
		case 13 : return 'YEAR';
		case 16 : return 'BIT';
		case 246 : return 'DECIMAL/NUMERIC/FIXED';
		case 252 : return 'BLOB/TEXT';
		case 253 : return 'VARCHAR/VARBINARY';
		case 254 : return 'CHAR/SET/ENUM/BINARY';
		default : return '?';
	}

}

function att_to_nom ($att)
{
	switch($att)
	{
		case 'numLicence' : return 'Numero de licence';
		case 'nomAdhérant' : return 'Nom';
		case 'prenomAdhérant' : return 'Prénom';
		case 'dateNaissanceAdhérant' : return 'Date de Naissance';
		case 'idA' : return "identifiant d'adresse";
		case 'idE' : return "Identifiant Ecole";
		case 'idCours' : return "Identifiant Cours";
		case 'libellé' : return "Libellé";
		case 'categorie_age' : return "Catégorie d'âge";
		case 'codeCompet' : return "Code";
		case 'libelléCompet' : return "libellé";
		case 'niveauCompet' : return "Niveau";
		case 'ville_organisatrice': return "Ville";
		case 'nom' : return "Nom";
		case 'prénom' :return "Prénom";
		case 'dateNaiss' : return "Date de Naissance";
		case 'idS': return "Structure";
		case 'nb_ecole': return "Nombre d'école";
		case 'code': return "Code";
		case 'nomComité': return "Nom";
		default : return $att;
	}
}



function get_instances_by_requete($requete)
{
	global $connexion;

	// récupération des informations sur la table (schema + instance)
 	$res = mysqli_query($connexion, $requete);  
	if($res == false)
	{
		$schema = null;
		$instances = null;
	} else
	{
		// extraction des informations sur le schéma à partir du résultat précédent
	   $infos_atts = mysqli_fetch_fields($res); 
	
	   // filtrage des information du schéma pour ne garder que le nom de l'attribut
	   $schema = array();
	   foreach( $infos_atts as $att ){
		   array_push( $schema , array( 'nom' => $att->{'name'} ) ); // syntaxe objet permettant de récupérer la propriété 'name' du de l'objet descriptif de l'attribut courant
	   }
	
	   // récupération des données (instances) de la table
	   $instances = mysqli_fetch_all($res, MYSQLI_ASSOC);
	}
	// renvoi d'un tableau contenant les informations sur le schéma (nom d'attribut) et les n-uplets
	return array('schema'=> $schema , 'instances'=> $instances);
}


function get_nbFedCom()
{
	return get_instances_by_requete(
        "SELECT A.nbFede,B.nbCR,C.nbCD 
        FROM (SELECT count(*) as nbFede FROM Fédération ) A,
            (SELECT count(*) as nbCR FROM Comité WHERE niveau = 'reg') B,
            (SELECT count(*) as nbCD FROM Comité WHERE niveau = 'dept') C ;");
}

function get_top5_ecole()
{
	return get_instances_by_requete(
        "SELECT E.nomEcole,A.ville,count(numLicence) as nb_inscrits 
            FROM est_licencié JOIN Ecole E USING(idE) JOIN Adresse A ON E.idE = A.idA
            WHERE année = '2022' GROUP BY idE ORDER BY nb_inscrits DESC LIMIT 5;");
}

function get_nbAd_annee($annee,$ecole)
{
	$res =  get_instances_by_requete("SELECT DISTINCT COUNT(numLicence) as nbInscrits FROM est_licencié WHERE année = ".$annee." AND idE = ".$ecole.";");
	return $res['instances'][0]['nbInscrits'];
}

function get_nbAd_compet()
{
	$res = get_instances_by_requete("SELECT count(B.numLicence) as nb
	FROM (SELECT DISTINCT A1.numLicence
		FROM couple_participe C1 JOIN Adhérant A1 ON C1.numLicence_1 = A1.numLicence
		UNION
		SELECT DISTINCT A2.numLicence
		FROM couple_participe C2 JOIN Adhérant A2 ON C2.numLicence_2 = A2.numLicence
		UNION
		SELECT DISTINCT A3.numLicence as nb
		FROM groupe_participe GP JOIN est_dans_groupe G ON G.idGroupe = GP.idGroupe JOIN Adhérant A3 ON A3.numLicence = G.numLicence) B;");
	return $res['instances'][0]['nb'];
}

function get_nbEcoleDeptFR()
{
	return get_instances_by_requete("SELECT A.code,count(E.nomEcole) as nb_ecole 
	FROM (SELECT DISTINCT (CASE LEFT(AD.codPos, 2) WHEN '97'
			THEN LEFT(AD.codPos,3)
			ELSE LEFT(AD.codPos, 2) END) as code,AD.idA
		FROM Adresse AD
		WHERE AD.pays = 'France') A
	JOIN Ecole E ON A.idA = E.idA
	GROUP BY A.code
	ORDER BY A.code;");
}

function get_comiteReg()
{
	return get_instances_by_requete("SELECT C.nomComité
	FROM Comité C JOIN Fédération F ON C.idF = F.idF
	WHERE F.nomFede = 'Fédération Française de Danse' AND C.niveau = 'reg'
	ORDER BY C.nomComité DESC;");
}


function get_responsables()
{
	return get_instances_by_requete("SELECT DISTINCT * FROM `Ecole` ORDER BY fondateurEcole;");
}

function get_infos_ecole($idEcole)
{
	return get_instances_by_requete("SELECT e.nomEcole as nomEcole, a.numVoie as numVoie, a.rue as rue , a.codPos as codPos, a.ville as ville FROM Ecole e JOIN Adresse a USING(idA) WHERE e.idE =".$idEcole." ;");
}

function get_employe($idEcole)
{
	return get_instances_by_requete("SELECT DISTINCT em.prenomEmployé as Prénom, em.nomEmployé as Nom  FROM Employé em JOIN enseigne USING(idEm) JOIN Ecole e USING(idE) WHERE e.idE = ".$idEcole.";");
}

function get_cours_ecole($idEcole)
{
	return get_instances_by_requete("SELECT libellé,categorie_age FROM `Cours` WHERE idE =".$idEcole.";");
}

function get_ad_ecole($idEcole)
{
	return get_instances_by_requete("SELECT DISTINCT a.numLicence, a.nomAdhérant, a.prenomAdhérant, a.dateNaissanceAdhérant, ass.année as Année FROM Adhérant a JOIN est_licencié ass USING(numLicence) JOIN Ecole e USING(idE) WHERE e.idE =".$idEcole.";");
}

function ajouter_ad_cours($idEcole,$idCours,$identifiant)
{
	global $connexion;
	mysqli_query($connexion,"INSERT INTO est_inscrit VALUES (".$idEcole.",".$identifiant.",".$idCours." ) ;");
}

function supprimer_ad_cours($idEcole,$idCours,$identifiant)
{
	global $connexion;
	mysqli_query($connexion,"DELETE from est_inscrit WHERE idCours=".$idCours." AND  idE=".$idEcole." AND  numLicence=".$identifiant.";");
}

function get_ad_filtre($idEcole,$kw)
{
	return get_instances_by_requete("SELECT DISTINCT a.numLicence, a.nomAdhérant, a.prenomAdhérant, a.dateNaissanceAdhérant FROM Adhérant a JOIN est_licencié USING(numLicence) JOIN Ecole e USING(idE) WHERE e.idE=".$_GET['idEcole']." AND ".implode(" or ",$kw));
}

function get_infos_ad()
{
	return get_instances_by_requete("SELECT numLicence as Licence, nomAdhérant as Nom , prenomAdhérant as Prénom From Adhérant where true;");
}



function modifier_edition($idCom,$annee   ,$ville)
{
	global $connexion;
	return  mysqli_query($connexion,'UPDATE Edition SET ville_organisatrice = "'.$ville.'" WHERE codeCompet = "'.$idCom.'" AND année = '.$annee.';');
}



function get_nom_type_struct($idS)
{

	$reussi = get_instances_by_requete("SELECT nomStruct, typeStruct FROM StructureSport where idS = ".$idS.";");
	if($reussi == null)
	{
		return array("nom"=>"Default","type"=>"stade");;
	}
	else
	{
		$res = array("nom"=>$reussi['instances'][0]["nomStruct"],"type"=>$reussi['instances'][0]["typeStruct"]);
		return $res;
	}
}

function modifier_structure($idS,$nomStruct,$typeStruct){
	global $connexion;
	return  mysqli_query($connexion,'UPDATE StructureSport SET nomStruct = "'.$nomStruct.'", typeStruct = "'.$typeStruct.'" WHERE idS = '.$idS.';');
}








function ajouter_couple($danseur1,$danseur2)
{
	global $connexion;
	//var_dump('INSERT INTO danse_avec SELECT '.$danseur1.', '.$danseur2.' ON DUPLICATE KEY UPDATE numLicence_1 = '.$danseur1.', numLicence_2 = '.$danseur2.';');
	return  mysqli_query($connexion,'INSERT INTO danse_avec VALUES ('.$danseur1.', '.$danseur2.') ON DUPLICATE KEY UPDATE numLicence_1 = '.$danseur1.', numLicence_2 = '.$danseur2.';');
}

function inscrire_couple($danseur1,$danseur2,$idCom,$annee)
{
	global $connexion;
	//var_dump('INSERT INTO couple_participe(numLicence_1,numLicence_2,codeCompet,année) VALUES ('.$danseur1.', '.$danseur2.',"'.$idCom.'", '.$annee.') ON DUPLICATE KEY UPDATE numLicence_1 = '.$danseur1.', numLicence_2 = '.$danseur2.',  codeCompet = "'.$idCom.'", année = '.$annee.';');
	return mysqli_query($connexion,'INSERT INTO couple_participe(numLicence_1,numLicence_2,codeCompet,année) VALUES ('.$danseur1.', '.$danseur2.',"'.$idCom.'", '.$annee.') ON DUPLICATE KEY UPDATE numLicence_1 = '.$danseur1.', numLicence_2 = '.$danseur2.',  codeCompet = "'.$idCom.'", année = '.$annee.';');
}


function get_couples($idCom)
{
	return get_instances_by_requete("SELECT a1.numLicence as Licence1, a1.nomAdhérant as nom1,  a2.numLicence as Licence2, a2.nomAdhérant as nom2
	FROM couple_participe cp 
	JOIN Adhérant a1 ON a1.numLicence = cp.numLicence_1 
	JOIN Adhérant a2 ON a2.numLicence = cp.numLicence_2
	WHERE codeCompet = '".$idCom."';");
}

function get_edition_from_couple($couple,$idCom)
{
	$couple = explode(" ", $couple); //danseur 1 = couple[0], danseur 2 = couple[1]
	return get_instances_by_requete("SELECT année, rang_final as rf FROM couple_participe WHERE numLicence_1 = ".$couple[0]." AND numLicence_2 = ".$couple[1]." AND codeCompet = '".$idCom."';");
}

function modifier_rang_couple($couple,$idCom,$annee,$rang)
{
	global $connexion;
	$couple = explode(" ", $couple); //danseur 1 = couple[0], danseur 2 = couple[1]
	mysqli_query($connexion,'INSERT INTO Classement(rang_final) VALUES ('.$rang.') ON DUPLICATE KEY UPDATE rang_final = '.$rang.'');
	return mysqli_query($connexion,'UPDATE couple_participe 
										SET rang_final = '.$rang.' 
											WHERE numLicence_1 = '.$couple[0].' AND 
												numLicence_2 = '.$couple[1].' AND
												codeCompet = "'.$idCom.'" AND
												année = '.$annee.';');
}




function get_groupes()
{
	return get_instances_by_requete("SELECT idGroupe, nomGroupe as nom FROM Groupe WHERE true;");
}

function get_couples_inscrits($idCom)
{
	return get_instances_by_requete("SELECT g.idGroupe,g.nomGroupe as nom  FROM groupe_participe gp JOIN Groupe g USING(idGroupe) WHERE gp.codeCompet = '".$idCom."';");
}

function inscrire_groupe($groupe,$idCom,$annee)
{
	global $connexion;
	return mysqli_query($connexion,"INSERT INTO groupe_participe (idGroupe,codeCompet,année) VALUES (".$groupe.",'".$idCom."', ".$annee.") ON DUPLICATE KEY UPDATE idGroupe = ".$groupe.";");
}

function modifier_rang_groupe($groupe,$idCom,$annee, $rang)
{
	global $connexion;
	mysqli_query($connexion,'INSERT INTO Classement(rang_final) VALUES ('.$rang.') ON DUPLICATE KEY UPDATE rang_final = '.$rang.'');
	return mysqli_query($connexion,'UPDATE groupe_participe 
										SET rang_final = '.$rang.' 
											WHERE idGroupe = '.$groupe.' AND 
												codeCompet = "'.$idCom.'" AND
												année = '.$annee.';');
}

function get_edition_from_groupe($groupe,$idCom)
{
	return get_instances_by_requete("SELECT année, rang_final as rf FROM groupe_participe WHERE idGroupe = ".$groupe." AND codeCompet = '".$idCom."';");
}

?>


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `Adhérant` (
  `numLicence` int(11) NOT NULL,
  `nomAdhérant` varchar(50) DEFAULT NULL,
  `prenomAdhérant` varchar(50) DEFAULT NULL,
  `dateNaissanceAdhérant` varchar(50) DEFAULT NULL,
  `idA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Adresse` (
  `idA` int(11) NOT NULL,
  `numVoie` int(11) DEFAULT NULL,
  `rue` varchar(50) DEFAULT NULL,
  `codPos` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `comRue` varchar(50) DEFAULT NULL,
  `pays` varchar(50) NOT NULL DEFAULT 'FRANCE',
  `boitePos` varchar(50) DEFAULT NULL,
  `cedex` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `a_pour_type` (
  `idD` int(11) NOT NULL,
  `idE` int(11) NOT NULL,
  `idCours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Classement` (
  `rang_final` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Comité` (
  `idC` int(11) NOT NULL,
  `idF` int(11) DEFAULT NULL,
  `nomComité` varchar(50) DEFAULT NULL,
  `niveau` varchar(50) DEFAULT NULL,
  `idC_1` int(11) DEFAULT NULL,
  `idA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Compétition` (
  `codeCompet` varchar(50) NOT NULL,
  `niveauCompet` varchar(50) DEFAULT NULL,
  `libelléCompet` varchar(50) DEFAULT NULL,
  `idF` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `couple_participe` (
  `numLicence_1` int(11) NOT NULL,
  `numLicence_2` int(11) NOT NULL,
  `codeCompet` varchar(50) NOT NULL,
  `année` varchar(50) NOT NULL,
  `rang_final` int(11) DEFAULT NULL,
  `numPassage` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Cours` (
  `idCours` int(11) NOT NULL,
  `idE` int(11) NOT NULL,
  `libellé` varchar(50) DEFAULT NULL,
  `categorie_age` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `CoursDanse` (
  `idCours` int(11) NOT NULL,
  `idE` int(11) NOT NULL,
  `catégorie` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `danse_avec` (
  `numLicence_1` int(11) NOT NULL,
  `numLicence_2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Ecole` (
  `idE` int(11) NOT NULL,
  `nomEcole` varchar(50) DEFAULT NULL,
  `fondateurEcole` varchar(100) DEFAULT NULL,
  `idF` int(11) NOT NULL,
  `idA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Edition` (
  `codeCompet` varchar(50) NOT NULL,
  `année` varchar(50) NOT NULL,
  `ville_organisatrice` varchar(50) DEFAULT NULL,
  `idS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Employé` (
  `idEm` int(11) NOT NULL,
  `nomEmployé` varchar(50) DEFAULT NULL,
  `prenomEmployé` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `enseigne` (
  `idEm` int(11) NOT NULL,
  `année` varchar(50) DEFAULT NULL,
  `idCours` int(11) NOT NULL,
  `idE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `EspaceDanse` (
  `idE` int(11) NOT NULL,
  `numeroSalle` int(11) NOT NULL,
  `typeAeration` varchar(50) DEFAULT NULL,
  `typeChaufffage` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `est_dans_groupe` (
  `numLicence` int(11) NOT NULL,
  `idGroupe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `est_gérée` (
  `idC` int(11) NOT NULL,
  `codeCompet` varchar(50) NOT NULL,
  `année` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `est_influence_par` (
  `idD` int(11) NOT NULL,
  `idD_influence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `est_inscrit` (
  `idE` int(11) NOT NULL,
  `numLicence` int(11) NOT NULL,
  `idCours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `est_licencié` (
  `idE` int(11) NOT NULL,
  `numLicence` int(11) NOT NULL,
  `année` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Eveil` (
  `idCours` int(11) NOT NULL,
  `idE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Fédération` (
  `idF` int(11) NOT NULL,
  `nomFede` varchar(50) DEFAULT NULL,
  `sigleFede` varchar(50) DEFAULT NULL,
  `présidentFede` varchar(50) DEFAULT NULL,
  `idA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Groupe` (
  `idGroupe` int(11) NOT NULL,
  `nomGroupe` varchar(50) DEFAULT NULL,
  `genre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `groupe_participe` (
  `idGroupe` int(11) NOT NULL,
  `codeCompet` varchar(50) NOT NULL,
  `année` varchar(50) NOT NULL,
  `rang_final` int(11) DEFAULT NULL,
  `numPassage` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `MembreComité` (
  `idMem` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `dateNaissance` varchar(50) DEFAULT NULL,
  `fonction` varchar(50) NOT NULL,
  `idC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Période` (
  `année` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Salle` (
  `idE` int(11) NOT NULL,
  `numeroSalle` int(11) NOT NULL,
  `nomSalle` varchar(50) DEFAULT NULL,
  `superficieSalle` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `StructureSport` (
  `idS` int(11) NOT NULL,
  `nomStruct` varchar(50) DEFAULT NULL,
  `typeStruct` varchar(50) DEFAULT NULL,
  `idA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Séance` (
  `idE` int(11) NOT NULL,
  `idCours` int(11) NOT NULL,
  `numSéance` int(11) NOT NULL,
  `jour` varchar(50) DEFAULT NULL,
  `créneau_horaire` varchar(50) DEFAULT NULL,
  `nb_max_inscrits` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `travaille` (
  `idE` int(11) NOT NULL,
  `idEm` int(11) NOT NULL,
  `année` varchar(50) DEFAULT NULL,
  `métier` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `TypeDanse` (
  `idD` int(11) NOT NULL,
  `nomDanse` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Vestiaires` (
  `idE` int(11) NOT NULL,
  `numeroSalle` int(11) NOT NULL,
  `mixte` varchar(50) DEFAULT NULL,
  `avec_douches` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `Zumba` (
  `idCours` int(11) NOT NULL,
  `idE` int(11) NOT NULL,
  `ambiance` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `Adhérant`
  ADD PRIMARY KEY (`numLicence`),
  ADD KEY `idA` (`idA`);

ALTER TABLE `Adresse`
  ADD PRIMARY KEY (`idA`);

  ALTER TABLE `a_pour_type`
  ADD PRIMARY KEY (`idD`,`idCours`,`idE`),
  ADD KEY `idCours` (`idCours`,`idE`);

ALTER TABLE `Classement`
  ADD PRIMARY KEY (`rang_final`);

ALTER TABLE `Comité`
  ADD PRIMARY KEY (`idC`),
  ADD KEY `idA` (`idA`),
  ADD KEY `idF` (`idF`),
  ADD KEY `idC_1` (`idC_1`);


ALTER TABLE `Compétition`
  ADD PRIMARY KEY (`codeCompet`),
  ADD KEY `idF` (`idF`);


ALTER TABLE `couple_participe`
  ADD PRIMARY KEY (`numLicence_1`,`numLicence_2`,`codeCompet`,`année`),
  ADD KEY `rang_final` (`rang_final`),
  ADD KEY `CONTRAINTE` (`codeCompet`,`année`);


ALTER TABLE `Cours`
  ADD PRIMARY KEY (`idCours`,`idE`),
  ADD KEY `idE` (`idE`);

ALTER TABLE `CoursDanse`
  ADD PRIMARY KEY (`idCours`,`idE`);

ALTER TABLE `danse_avec`
  ADD PRIMARY KEY (`numLicence_1`,`numLicence_2`),
  ADD UNIQUE KEY `numLicence_1` (`numLicence_1`,`numLicence_2`),
  ADD KEY `numLicence_2` (`numLicence_2`);

ALTER TABLE `Ecole`
  ADD PRIMARY KEY (`idE`),
  ADD KEY `idF` (`idF`),
  ADD KEY `idA` (`idA`);


ALTER TABLE `Edition`
  ADD PRIMARY KEY (`codeCompet`,`année`),
  ADD KEY `idS` (`idS`);

ALTER TABLE `Employé`
  ADD PRIMARY KEY (`idEm`);

ALTER TABLE `enseigne`
  ADD PRIMARY KEY (`idEm`,`idCours`,`idE`),
  ADD KEY `année` (`année`),
  ADD KEY `idCours` (`idCours`,`idE`);


ALTER TABLE `EspaceDanse`
  ADD PRIMARY KEY (`idE`,`numeroSalle`);

ALTER TABLE `est_dans_groupe`
  ADD PRIMARY KEY (`numLicence`,`idGroupe`),
  ADD KEY `idGroupe` (`idGroupe`);


ALTER TABLE `est_gérée`
  ADD PRIMARY KEY (`idC`,`codeCompet`,`année`),
  ADD KEY `codeCompet` (`codeCompet`,`année`);


ALTER TABLE `est_influence_par`
  ADD PRIMARY KEY (`idD`,`idD_influence`),
  ADD KEY `idD_influence` (`idD_influence`);


ALTER TABLE `est_inscrit`
  ADD PRIMARY KEY (`idE`,`idCours`,`numLicence`),
  ADD KEY `numLicence` (`numLicence`);


ALTER TABLE `est_licencié`
  ADD PRIMARY KEY (`idE`,`numLicence`),
  ADD KEY `année` (`année`),
  ADD KEY `numLicence` (`numLicence`);


ALTER TABLE `Eveil`
  ADD PRIMARY KEY (`idCours`,`idE`);


ALTER TABLE `Fédération`
  ADD PRIMARY KEY (`idF`),
  ADD KEY `idA` (`idA`);


ALTER TABLE `Groupe`
  ADD PRIMARY KEY (`idGroupe`);


ALTER TABLE `groupe_participe`
  ADD PRIMARY KEY (`idGroupe`,`codeCompet`,`année`),
  ADD KEY `codeCompet` (`codeCompet`,`année`),
  ADD KEY `rang_final` (`rang_final`);


ALTER TABLE `MembreComité`
  ADD PRIMARY KEY (`idMem`,`idC`),
  ADD KEY `idC` (`idC`);


ALTER TABLE `Période`
  ADD PRIMARY KEY (`année`);

ALTER TABLE `Salle`
  ADD PRIMARY KEY (`idE`,`numeroSalle`);


ALTER TABLE `StructureSport`
  ADD PRIMARY KEY (`idS`),
  ADD KEY `idA` (`idA`);


ALTER TABLE `Séance`
  ADD PRIMARY KEY (`idE`,`idCours`,`numSéance`);


ALTER TABLE `travaille`
  ADD PRIMARY KEY (`idE`,`idEm`),
  ADD KEY `idEm` (`idEm`),
  ADD KEY `année` (`année`);


ALTER TABLE `TypeDanse`
  ADD PRIMARY KEY (`idD`);


ALTER TABLE `Vestiaires`
  ADD PRIMARY KEY (`idE`,`numeroSalle`);


ALTER TABLE `Zumba`
  ADD PRIMARY KEY (`idCours`,`idE`);


ALTER TABLE `Adresse`
  MODIFY `idA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=640;


ALTER TABLE `Comité`
  MODIFY `idC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;


ALTER TABLE `Ecole`
  MODIFY `idE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;


ALTER TABLE `Employé`
  MODIFY `idEm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;


ALTER TABLE `Fédération`
  MODIFY `idF` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;


ALTER TABLE `Groupe`
  MODIFY `idGroupe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;


ALTER TABLE `MembreComité`
  MODIFY `idMem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;


ALTER TABLE `StructureSport`
  MODIFY `idS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;


ALTER TABLE `TypeDanse`
  MODIFY `idD` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;


ALTER TABLE `Adhérant`
  ADD CONSTRAINT `Adhérant_ibfk_1` FOREIGN KEY (`idA`) REFERENCES `Adresse` (`idA`);


ALTER TABLE `a_pour_type`
  ADD CONSTRAINT `a_pour_type_ibfk_1` FOREIGN KEY (`idD`) REFERENCES `TypeDanse` (`idD`),
  ADD CONSTRAINT `a_pour_type_ibfk_2` FOREIGN KEY (`idCours`,`idE`) REFERENCES `CoursDanse` (`idCours`, `idE`);


ALTER TABLE `Comité`
  ADD CONSTRAINT `Comité_ibfk_1` FOREIGN KEY (`idA`) REFERENCES `Adresse` (`idA`),
  ADD CONSTRAINT `Comité_ibfk_2` FOREIGN KEY (`idF`) REFERENCES `Fédération` (`idF`),
  ADD CONSTRAINT `Comité_ibfk_3` FOREIGN KEY (`idC_1`) REFERENCES `Comité` (`idC`);


ALTER TABLE `Compétition`
  ADD CONSTRAINT `Compétition_ibfk_1` FOREIGN KEY (`idF`) REFERENCES `Fédération` (`idF`);


ALTER TABLE `couple_participe`
  ADD CONSTRAINT `couple_participe_ibfk_1` FOREIGN KEY (`numLicence_1`,`numLicence_2`) REFERENCES `danse_avec` (`numLicence_1`, `numLicence_2`),
  ADD CONSTRAINT `couple_participe_ibfk_2` FOREIGN KEY (`codeCompet`,`année`) REFERENCES `Edition` (`codeCompet`, `année`),
  ADD CONSTRAINT `couple_participe_ibfk_3` FOREIGN KEY (`rang_final`) REFERENCES `Classement` (`rang_final`),
  ADD CONSTRAINT `couple_participe_ibfk_4` FOREIGN KEY (`codeCompet`,`année`) REFERENCES `Edition` (`codeCompet`, `année`),
  ADD CONSTRAINT `couple_participe_ibfk_5` FOREIGN KEY (`codeCompet`,`année`) REFERENCES `Edition` (`codeCompet`, `année`) ON UPDATE CASCADE;


ALTER TABLE `Cours`
  ADD CONSTRAINT `Cours_ibfk_1` FOREIGN KEY (`idE`) REFERENCES `Ecole` (`idE`);


ALTER TABLE `CoursDanse`
  ADD CONSTRAINT `CoursDanse_ibfk_1` FOREIGN KEY (`idCours`,`idE`) REFERENCES `Cours` (`idCours`, `idE`);


ALTER TABLE `danse_avec`
  ADD CONSTRAINT `danse_avec_ibfk_1` FOREIGN KEY (`numLicence_1`) REFERENCES `Adhérant` (`numLicence`),
  ADD CONSTRAINT `danse_avec_ibfk_2` FOREIGN KEY (`numLicence_2`) REFERENCES `Adhérant` (`numLicence`);


ALTER TABLE `Ecole`
  ADD CONSTRAINT `Ecole_ibfk_1` FOREIGN KEY (`idF`) REFERENCES `Fédération` (`idF`),
  ADD CONSTRAINT `Ecole_ibfk_2` FOREIGN KEY (`idA`) REFERENCES `Adresse` (`idA`);


ALTER TABLE `Edition`
  ADD CONSTRAINT `Edition_ibfk_1` FOREIGN KEY (`codeCompet`) REFERENCES `Compétition` (`codeCompet`),
  ADD CONSTRAINT `Edition_ibfk_2` FOREIGN KEY (`idS`) REFERENCES `StructureSport` (`idS`);


ALTER TABLE `enseigne`
  ADD CONSTRAINT `enseigne_ibfk_1` FOREIGN KEY (`idEm`) REFERENCES `Employé` (`idEm`),
  ADD CONSTRAINT `enseigne_ibfk_2` FOREIGN KEY (`année`) REFERENCES `Période` (`année`),
  ADD CONSTRAINT `enseigne_ibfk_3` FOREIGN KEY (`idCours`,`idE`) REFERENCES `Cours` (`idCours`, `idE`);


ALTER TABLE `EspaceDanse`
  ADD CONSTRAINT `EspaceDanse_ibfk_1` FOREIGN KEY (`idE`,`numeroSalle`) REFERENCES `Salle` (`idE`, `numeroSalle`);


ALTER TABLE `est_dans_groupe`
  ADD CONSTRAINT `est_dans_groupe_ibfk_1` FOREIGN KEY (`numLicence`) REFERENCES `Adhérant` (`numLicence`),
  ADD CONSTRAINT `est_dans_groupe_ibfk_2` FOREIGN KEY (`idGroupe`) REFERENCES `Groupe` (`idGroupe`);


ALTER TABLE `est_gérée`
  ADD CONSTRAINT `est_gérée_ibfk_1` FOREIGN KEY (`idC`) REFERENCES `Comité` (`idC`),
  ADD CONSTRAINT `est_gérée_ibfk_2` FOREIGN KEY (`codeCompet`,`année`) REFERENCES `Edition` (`codeCompet`, `année`);


ALTER TABLE `est_influence_par`
  ADD CONSTRAINT `est_influence_par_ibfk_1` FOREIGN KEY (`idD`) REFERENCES `TypeDanse` (`idD`),
  ADD CONSTRAINT `est_influence_par_ibfk_2` FOREIGN KEY (`idD_influence`) REFERENCES `TypeDanse` (`idD`);


ALTER TABLE `est_inscrit`
  ADD CONSTRAINT `est_inscrit_ibfk_1` FOREIGN KEY (`idE`,`idCours`) REFERENCES `Cours` (`idE`, `idCours`),
  ADD CONSTRAINT `est_inscrit_ibfk_2` FOREIGN KEY (`numLicence`) REFERENCES `Adhérant` (`numLicence`);


ALTER TABLE `est_licencié`
  ADD CONSTRAINT `est_licencié_ibfk_1` FOREIGN KEY (`idE`) REFERENCES `Ecole` (`idE`),
  ADD CONSTRAINT `est_licencié_ibfk_2` FOREIGN KEY (`année`) REFERENCES `Période` (`année`),
  ADD CONSTRAINT `est_licencié_ibfk_3` FOREIGN KEY (`numLicence`) REFERENCES `Adhérant` (`numLicence`);


ALTER TABLE `Eveil`
  ADD CONSTRAINT `Eveil_ibfk_1` FOREIGN KEY (`idCours`,`idE`) REFERENCES `Cours` (`idCours`, `idE`);


ALTER TABLE `Fédération`
  ADD CONSTRAINT `Fédération_ibfk_1` FOREIGN KEY (`idA`) REFERENCES `Adresse` (`idA`);


ALTER TABLE `groupe_participe`
  ADD CONSTRAINT `groupe_participe_ibfk_1` FOREIGN KEY (`idGroupe`) REFERENCES `Groupe` (`idGroupe`),
  ADD CONSTRAINT `groupe_participe_ibfk_2` FOREIGN KEY (`codeCompet`,`année`) REFERENCES `Edition` (`codeCompet`, `année`),
  ADD CONSTRAINT `groupe_participe_ibfk_3` FOREIGN KEY (`rang_final`) REFERENCES `Classement` (`rang_final`);


ALTER TABLE `MembreComité`
  ADD CONSTRAINT `MembreComité_ibfk_1` FOREIGN KEY (`idC`) REFERENCES `Comité` (`idC`);


ALTER TABLE `Salle`
  ADD CONSTRAINT `Salle_ibfk_1` FOREIGN KEY (`idE`) REFERENCES `Ecole` (`idE`);


ALTER TABLE `StructureSport`
  ADD CONSTRAINT `StructureSport_ibfk_1` FOREIGN KEY (`idA`) REFERENCES `Adresse` (`idA`);


ALTER TABLE `Séance`
  ADD CONSTRAINT `Séance_ibfk_1` FOREIGN KEY (`idE`,`idCours`) REFERENCES `Cours` (`idE`, `idCours`);


ALTER TABLE `travaille`
  ADD CONSTRAINT `travaille_ibfk_1` FOREIGN KEY (`idE`) REFERENCES `Ecole` (`idE`),
  ADD CONSTRAINT `travaille_ibfk_2` FOREIGN KEY (`idEm`) REFERENCES `Employé` (`idEm`),
  ADD CONSTRAINT `travaille_ibfk_3` FOREIGN KEY (`année`) REFERENCES `Période` (`année`);


ALTER TABLE `Vestiaires`
  ADD CONSTRAINT `Vestiaires_ibfk_1` FOREIGN KEY (`idE`,`numeroSalle`) REFERENCES `Salle` (`idE`, `numeroSalle`);


ALTER TABLE `Zumba`
  ADD CONSTRAINT `Zumba_ibfk_1` FOREIGN KEY (`idCours`,`idE`) REFERENCES `Cours` (`idCours`, `idE`);
COMMIT;

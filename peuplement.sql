INSERT INTO Adresse (numVoie,rue,codPos,ville)
SELECT DISTINCT adr_fede_numVoie,adr_fede_rue,adr_fede_cp,adr_fede_ville
FROM donnees_fournies.instances1 
WHERE true ;

INSERT INTO Adresse (numVoie,rue,codPos,ville)
SELECT DISTINCT adr_comite_dept_numVoie,adr_comite_dept_rue,adr_comite_dept_cp,adr_comite_dept_ville
FROM donnees_fournies.instances1 
WHERE  (adr_comite_dept_numVoie,adr_comite_dept_rue,adr_comite_dept_cp,adr_comite_dept_ville) NOT IN 
(SELECT numVoie,rue,codPos,ville FROM Adresse);

INSERT INTO Adresse (numVoie,rue,codPos,ville)
SELECT DISTINCT adr_comite_reg_numVoie,adr_comite_reg_rue,adr_comite_reg_cp,adr_comite_reg_ville
FROM donnees_fournies.instances1 
WHERE  (adr_comite_reg_numVoie,adr_comite_reg_rue,adr_comite_reg_cp,adr_comite_reg_ville) NOT IN 
(SELECT numVoie,rue,codPos,ville FROM Adresse);

INSERT INTO Adresse (numVoie,rue,codPos,ville)
SELECT DISTINCT adr_ecole_numVoie,adr_ecole_rue,adr_ecole_cp,adr_ecole_ville
FROM donnees_fournies.instances3
WHERE  (adr_ecole_numVoie,adr_ecole_rue,adr_ecole_cp,adr_ecole_ville) NOT IN 
(SELECT numVoie,rue,codPos,ville FROM Adresse);

INSERT INTO Adresse (numVoie,rue,codPos,ville)
SELECT DISTINCT adr_danseur_numVoie,adr_danseur_rue,adr_danseur_cp,adr_danseur_ville
FROM donnees_fournies.instances4
WHERE  (adr_danseur_numVoie,adr_danseur_rue,adr_danseur_cp,adr_danseur_ville) NOT IN 
(SELECT numVoie,rue,codPos,ville FROM Adresse);

INSERT INTO Fédération(nomFede,sigleFede,présidentFede,idA)
SELECT DISTINCT f.fede_nom,f.fede_sigle,f.fede_dirigeant,A.idA FROM donnees_fournies.instances1 f JOIN
Adresse A ON 
A.numVoie = f.adr_fede_numVoie 
AND A.rue = f.adr_fede_rue
AND A.codPos = f.adr_fede_cp
AND A.ville = f.adr_fede_ville ;

INSERT INTO Comité (idF,nomComité,niveau,idA) 
SELECT DISTINCT F.idF,C.comite_reg_nom,C.comite_reg_niveau,A.idA
FROM donnees_fournies.instances1 C JOIN Fédération F ON C.fede_nom = F.nomFede
JOIN Adresse A ON
A.numVoie = C.adr_comite_reg_numVoie 
AND A.rue = C.adr_comite_reg_rue
AND A.codPos = C.adr_comite_reg_cp
AND A.ville = C.adr_comite_reg_ville ;

INSERT INTO Comité (idF,nomComité,niveau,idC_1,idA) 
SELECT DISTINCT F.idF  ,C.comite_dept_nom,C.comite_dept_niveau,C1.idC,A.idA
FROM donnees_fournies.instances1 C JOIN Fédération F ON C.fede_nom = F.nomFede
JOIN Comité C1 ON C1.idF = F.idF AND C1.nomComité = C.comite_reg_nom AND C1.niveau = C.comite_reg_niveau
JOIN Adresse A ON
A.numVoie = C.adr_comite_dept_numVoie 
AND A.rue = C.adr_comite_dept_rue
AND A.codPos = C.adr_comite_dept_cp
AND A.ville = C.adr_comite_dept_ville
WHERE C.comite_reg_code_dept IS NOT NULL;

INSERT INTO Ecole(nomEcole,fondateurEcole,idF,idA)
SELECT DISTINCT I3.ecole_nom,I3.ecole_fondateur,F.idF,A.idA
FROM donnees_fournies.instances3 I3 
JOIN Fédération F 
ON I3.fede_nom = F.nomFede
JOIN Adresse A
ON I3.adr_ecole_numVoie = A.numVoie
AND I3.adr_ecole_rue = A.rue
AND I3.adr_ecole_cp = A.codPos
AND I3.adr_ecole_ville = A.ville
WHERE I3.ecole_fondateur NOT IN (SELECT fondateurEcole FROM Ecole);

INSERT INTO Employé(nomEmployé,prenomEmployé)
SELECT DISTINCT cours_resp_nom,cours_resp_prénom
FROM donnees_fournies.instances3;

INSERT INTO Période (année)
SELECT DISTINCT annee_inscription FROM donnees_fournies.instances4 WHERE true;

INSERT INTO TypeDanse (idD,nomDanse) 
SELECT DISTINCT idD,type_danse FROM donnees_fournies.type_danse 
WHERE true;

INSERT INTO Compétition (codeCompet,niveauCompet,libelléCompet,idF)
SELECT DISTINCT A.compet_code,A.compet_niveau,A.compet_libellé,F.idF 
FROM donnees_fournies.instances2 A JOIN Fédération F ON A.fede_nom = F.nomFede ;

INSERT INTO StructureSport (idA,nomStruct,typeStruct) VALUES
(52,'Kaaris','gymnase'),
(77,'Booba','palais des sports'),
(84,'Ninho','gymnase'),
(73,'SCH','palais des sports'),
(39,'Jul','gymnase'),
(35,'Soolking','palais des sports');

INSERT INTO Adhérant(numLicence,nomAdhérant,prenomAdhérant,dateNaissanceAdhérant,idA)
SELECT DISTINCT I4.danseur_numLicence,I4.danseur_nom,I4.danseur_prenom,I4.danseur_date_naissance,A.idA
FROM donnees_fournies.instances4 I4 JOIN Adresse A
ON A.numVoie = I4.adr_danseur_numVoie
AND A.rue = I4.adr_danseur_rue
AND A.codPos = I4.adr_danseur_cp
AND A.ville = I4.adr_danseur_ville;

INSERT INTO Cours(idCours,libellé,categorie_age,idE)
SELECT DISTINCT A.cours_code,A.cours_libellé,A.cours_categorie_age,C.idE 
FROM donnees_fournies.instances3 A JOIN Adresse B ON B.numVoie = A.adr_ecole_numVoie
AND B.rue = A.adr_ecole_rue AND B.codPos = A.adr_ecole_cp AND B.ville = A.adr_ecole_ville JOIN Ecole C ON B.idA = C.idA ;

INSERT INTO CoursDanse(idCours,idE)
SELECT DISTINCT idCours,idE FROM Cours
WHERE NOT (libellé LIKE "%Zumba%");

INSERT INTO Zumba(idCours,idE)
SELECT DISTINCT idCours,idE FROM Cours
WHERE libellé LIKE "%Zumba%";

INSERT INTO Edition (codeCompet,année,ville_organisatrice)
SELECT DISTINCT C.codeCompet,E.edition_année,E.edition_ville_orga
FROM donnees_fournies.instances2 E JOIN Compétition C 
ON C.libelléCompet = E.compet_libellé AND C.codeCompet = E.compet_code AND C.niveauCompet = E.compet_niveau;

INSERT INTO MembreComité (idC,prenom,nom,dateNaissance)
VALUES (1,'Yoru','Uchiwa','10/10/1990'),(1,'Reyna','Reyes','23/01/1986'),(1,'Jett','Park',15/11/1996),(1,'Sova','Ruskov',22/06/2000),
(1,'Joy','Jaeger','05/02/1989'),(2,'Raze','DoSantos','12/12/2002'),(2,'Neon','Nguyen','09/04/1978'),(2,'Phoenix','Traoré','18/06/1999'),
(2,'Astra','VoiLacté','19/07/1998'),(2,'Skye','Sydney','30/01/1982'),(2,'Fade','Durum','16/08/2000'),(2,'Harbor','Bolywood','01/09/1975');

INSERT INTO Groupe (nomGroupe,genre) VALUES
('Equipe2chok','masculin'),('Raya','masculin'),('PowerOukhty','feminin'),
('DBZ','mixte'),('SNK','mixte'),('TahSah','masculin'),('Jdid','mixte');

INSERT INTO Classement (rang_final) SELECT DISTINCT A.rang_final FROM donnees_fournies.instances2 A WHERE true;

INSERT INTO danse_avec (numLicence_1,numLicence_2)
SELECT DISTINCT A.danseur_numLicence1,A.danseur_numLicence2
FROM donnees_fournies.instances2 A WHERE true;

INSERT INTO est_dans_groupe (idGroupe,numLicence)
SELECT G.idGroupe,A.numLicence FROM (SELECT idGroupe FROM Groupe ORDER BY RAND() LIMIT 7)G,
(SELECT numLicence FROM Adhérant ORDER BY RAND() LIMIT 7)A
ORDER BY RAND()
LIMIT 20;

INSERT INTO groupe_participe (idGroupe,codeCompet,année,rang_final)
SELECT D.idGroupe,D.codeCompet,D.année,C.rang_final FROM
(SELECT DISTINCT A.idGroupe,B.codeCompet,B.année
FROM (SELECT DISTINCT idGroupe FROM Groupe ORDER BY RAND() LIMIT 7)A,
	 (SELECT DISTINCT codeCompet,année FROM Edition ORDER BY RAND() LIMIT 10)B
ORDER BY RAND()
LIMIT 18)D,(SELECT DISTINCT rang_final FROM Classement ORDER BY RAND() LIMIT 10)C
ORDER BY RAND()
LIMIT 10;

INSERT INTO couple_participe (numLicence_1,numLicence_2,codeCompet,année,rang_final)
SELECT DISTINCT DA.numLicence_1,DA.numLicence_2,E.codeCompet,E.année,C.rang_final
FROM donnees_fournies.instances2 A 
	JOIN danse_avec DA ON A.danseur_numLicence1 = DA.numLicence_1 AND A.danseur_numLicence2 = DA.numLicence_2 
   JOIN Edition E ON E.codeCompet = A.compet_code AND E.année = A.edition_année
   JOIN Classement C ON C.rang_final = A.rang_final;

INSERT INTO travaille (idEm,idE)
SELECT DISTINCT A.idEm,C.idE
FROM Employé A JOIN 
(SELECT S.ecole_nom,S.ecole_fondateur,S.cours_resp_nom,S.cours_resp_prénom,S.cours_code FROM donnees_fournies.instances3 S WHERE true) B
ON A.nomEmployé = B.cours_resp_nom AND A.prenomEmployé = B.cours_resp_prénom 
JOIN Ecole C ON C.nomEcole = B.ecole_nom AND C.fondateurEcole = B.ecole_fondateur WHERE true;

INSERT INTO enseigne (idEm,idCours,idE)
SELECT DISTINCT A.idEm,D.idCours,C.idE
FROM Employé A JOIN 
(SELECT S.ecole_nom,S.ecole_fondateur,S.cours_resp_nom,S.cours_resp_prénom,S.cours_code FROM donnees_fournies.instances3 S WHERE true) B
ON A.nomEmployé = B.cours_resp_nom AND A.prenomEmployé = B.cours_resp_prénom 
JOIN Ecole C ON C.nomEcole = B.ecole_nom AND C.fondateurEcole = B.ecole_fondateur JOIN Cours D ON D.idCours = B.cours_code
WHERE true;

INSERT INTO est_gérée (idC,codeCompet,année)
VALUES (1,'CFDH','2020'),(1,'CFDH','2021'),(1,'CFDH','2022'),(1,'CFDH','2023'),(1,'CFDH','2024'),(1,'CFDH','2025'),(2,'CFDJ','2020'),(2,'CFDJ','2021'),(2,'CFDJ','2022'),
(3,'CFDS','2020'),(3,'CFDS','2021'),(3,'CFDS','2022'),(4,'CFL','2020'),(4,'CFL','2021'),(4,'CFL','2022'),(2,'CFR','2020'),(2,'CFR','2021'),(2,'CFR','2022');

INSERT INTO est_licencié (numLicence,idE,année)
SELECT DISTINCT A.numLicence,E.idE,P.année FROM Adhérant A JOIN
(SELECT DISTINCT danseur_numLicence,ecole_nom,adr_ecole_ville,annee_inscription FROM donnees_fournies.instances4 WHERE true) B
ON A.numLicence = B.danseur_numLicence JOIN Ecole E ON E.nomEcole = B.ecole_nom 
JOIN Adresse C ON C.ville = B.adr_ecole_ville AND C.idA = E.idA JOIN Période P ON P.année = B.annee_inscription;


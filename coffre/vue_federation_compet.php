<div class="page">
    <div class="titre_page">
        <h1>Gestion des compétitions</h1>
    </div>
    <?php
        if($message == "Il n'y a pas d'adhérant inscrits dans cette école.")
        {
    ?>
            <div class="msg_resultat_ins">
                <p><?= $message ?></p>
            </div>
    <?php
        }
        else
        { // Partie où les adhérants sont afficher avec la barre de recherche.
        
    ?>
        <?php
            if ( !isset($_GET['idCom']) && !isset($_GET['ajouter']))
            { ?>
                <ul class="links">
                    <li><a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&ajouter=#">Ajouter une nouvelle compétition</a></li>
                </ul>
                <div class="filtre"> 
                    <form class="filtre" action="#" method="post">
                        <h2> Filtrer par </h2>
                        <select name="attributRecherche" id="attributRecherche">
                            <?php foreach($competition['schema'] as $att){ ?>
                                <option value="<?= $att['nom'] ?>"><?= att_to_nom($att['nom']) ?></option>
                            <?php }?>
                        </select>
                        <input type="text" name="mots_cles" placeholder="Entrez la recherche"/>
                        <input type="submit" name="boutonRechercher" value="Rechercher"/>
                    </form>
                </div>
            <?php } ?>
            <div class="msg_resultat_ins">
                <p>
                    <?= $message ?>
                </p>
            </div>
            <?php if(count($competition['instances']) != 0 && !isset($_GET['ajouter'])){ ?>
                <div class="res_com">
                    <table class="table_resultat">
                            <thead>
                                <tr>
                                <?php
                                    foreach($competition['schema'] as $att) {  // pour parcourir les attributs
                            
                                        echo '<th>';
                                            echo att_to_nom($att['nom']);
                                        echo '</th>';
                            
                                    }
                                    if( !isset($_GET['idCom'])){echo '<th> Cliquez pour visualiser </th>';echo '<th>Cliquez pour supprimer</th>';}
                                ?>	
                                </tr>	
                                </thead>
                            <tbody>

                            <?php
                            
                                foreach($competition['instances'] as $row) {  // pour parcourir les n-uplets
                            
                                echo '<tr>';
                                $keys = array_keys($row);
                                if(!isset($_GET['idCom'])){array_push($row,"Visualiser");array_push($row,"Supprimer");};
                                foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                    if($valeur == 'Visualiser')
                                    {
                                        echo '<td>'. '<a href="./index.php?page=federation_compet&idF='.$_GET['idF'].'&idCom='.$row[$keys[0]].'">Visualiser</a>' . '</td>';
                                    }
                                    else if($valeur == 'Supprimer')
                                    {
                                        echo '<td>'. '<a href="./index.php?page=federation_compet&idF='.$_GET['idF'].'&code='.$row[$keys[0]].'&supprimer=#">Supprimer</a>' . '</td>';

                                    }
                                    else 
                                    {
                                        echo '<td>'. $valeur . '</td>';
                                    }
                                }
                                echo '</tr>';
                            }
                        ?>
                        </tbody>
                    </table>
                </div>




            <?php }
            if(isset($_GET['idCom']))
            { 
                if(!isset($_GET['modifier']) && !isset($_GET['ajouter'])){ // Bloc d'accueil d'une compétition selectionné
                    ?>
            <div style="display: flex; justify-content: space-around; padding: 30px;">
                <div class="bloc_compet">
                    <div class="msg_resultat_ins">
                            <p><?= (count($edition['instances']) == 0 ? "Pas d'édition enregistré.": count($edition['instances'])." éditions enregistrées") ?></p>
                    </div>
                    <?php 
                    if(count($edition['instances']) != 0)
                    { ?>
                        <div class="res_ins">
                            <table class="table_resultat">
                                <thead>
                                    <tr>
                                    <?php
                                            foreach($edition['schema'] as $att) {  // pour parcourir les attributs
                                    
                                                echo '<th>';
                                                    echo att_to_nom($att['nom']);
                                                echo '</th>';
                                    
                                            }
                                            echo '<th>Cliquez pour supprimer</th>';
                                            echo '<th>Cliquez pour modifier</th>';
                                        ?>	
                                        </tr>	
                                        </thead>
                                    <tbody>

                                    <?php
                                    
                                        foreach($edition['instances'] as $row) {  // pour parcourir les n-uplets
                                    
                                        echo '<tr>';
                                        $keys = array_keys($row);
                                        array_push($row,"Supprimer");
                                        array_push($row,"Modifier");
                                        foreach($row as $valeur) { // pour parcourir chaque valeur de n-uplets
                                            if($valeur == 'Supprimer')
                                            {
                                                echo '<td>'. '<a href="./index.php?page=federation_compet&idF='.$_GET['idF'].'&idCom='.$_GET['idCom'].'&annee='.$row['année'].'&supprimer=#">Supprimer</a>' . '</td>';
                                            }else if($valeur == 'Modifier')
                                            {
                                                echo '<td>'. '<a href="./index.php?page=federation_compet&idF='.$_GET['idF'].'&idCom='.$_GET['idCom'].'&annee='.$row['année'].'&ville='.$row['ville_organisatrice'].'&modifier=#">Modifier</a>' . '</td>';
                                            }
                                            else
                                            {
                                                echo '<td>'. $valeur . '</td>';
                                            }
                                        }
                                        echo '</tr>';
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
                <div class="bloc_compet">
                    <p>Voici les options disponibles : </p>
                    <h2>
                        <ul class="links">
                            <li><a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>&ajouter=#">Ajouter une édition</a></li>
                            <li><a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>&modifier=#">Modifier les informations de la compétition</a></li>
                        </ul>
                    </h2>
                </div>
            </div>
            <?php }else{
            {
                if(isset($_GET['modifier'])) // Bloc des modifications 
                {
                    if(!isset($_GET['annee'])) // Modif de compétition
                {?>
                <div class="modif">
                    <h1>Modification de la compétition : </h1>
                </br>
                    <form action="#" class="form_accueil" method="post">
                        <select name="niveau" id="niveau">
                            <option value="National">National</option>
                            <option value="Départemental">Départemental</option>
                            <option value="Régional">Régional</option>
                        </select>
                        <input type="text" name="libelle" placeholder="Entrez le libellé" value=" <?= $competition['instances'][0]['libelléCompet'] ?>"/>
                        <input type="submit" value="Modifier" name="ModifierCom">
                    </form>
                    <div class="msg_resultat_ins">
                        <h1><?=$message_err?></h1>
                    </div> 
                    <div class = "retour">
                        <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>"> Retour </a>
                    </div>
                </div>

            <?php }else{ ?>
                <div class="modif">
                    <h1>Modification de l'édition : </h1>
                    </br>

                    <form action="#" class="form_accueil" method="post">
                        <label for="annee">Année </label>
                        <select name="annee" id="annee">
                            <?php for($i = 2050; $i >= 2023; $i--){ 
                                if(!in_array($i,$annee_prise)){  ?>
                                <option value="<?= $i ?>"> <?= $i ?> </option>
                            <?php }} ?>
                        </select>
                        <label for="annee">Ville organisatrice </label><input type="text" name="ville" placeholder="Entrez la ville" value = "<?= $_GET['ville'] ?>"/>
                        <input type="submit" value="Modifier" name="ModifierEd">
                </div>
                <div class="msg_resultat_ins">
                    <h1><?=$message_err?></h1>
                </div>                
                <div class = "retour">
                    <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>"> Retour </a>
                </div>
            <?php  }


            }
            if(isset($_GET['ajouter'])) { 
                if(isset($_GET['idCom'])){ // Bloc d'ajout d'une édition ?>
            <div class="ajout">
                <h1> Ajout d'une édition </h1>
                </br>

                <form action="#" class="form_accueil" method="post">
                    <label for="annee">Année </label>
                    <select name="annee" id="annee">
                        <?php for($i = 2050; $i >= 2023; $i--){ 
                            if(!in_array($i,$annee_prise)){  ?>
                            <option value="<?= $i ?>"> <?= $i ?> </option>
                        <?php }} ?>
                    </select>
                    <label for="annee">Ville organisatrice </label><input type="text" name="ville" placeholder="Entrez la ville"/>
                    <input type="submit" value="Ajouter" name="Ajouter">
                </form>
            </div>
            <div class="msg_resultat_ins">
                <h1><?=$message_err?></h1>
            </div> 
            <div class = "retour">
                <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>&idCom=<?= $_GET['idCom'] ?>"> Retour </a>
            </div>
            <?php 
                } ?>
            <?php
                }
                }
                }
                }
                
                if(isset($_GET['ajouter']) && !isset($_GET['idCom'])){ ?>
                <div class="ajout">
                    <h1>Ajout d'une nouvelle compétition : </h1>
                </br>
                    <form action="#" class="form_accueil" method="post">
                        <input type="text" name="code" placeholder="Entrez le code "/>
                        <select name="niveau" id="niveau">
                            <option value="National">National</option>
                            <option value="Départemental">Départemental</option>
                            <option value="Régional">Régional</option>
                        </select>
                        <input type="text" name="libelle" placeholder="Entrez le libellé" />
                        <input type="submit" value="Ajouter" name="Ajouter">
                    </form>
                    <div class="msg_resultat_ins">
                        <h1><?=$message_err?></h1>
                    </div> 
                    <div class = "retour">
                        <a href="./index.php?page=federation_compet&idF=<?= $_GET['idF'] ?>"> Retour </a>
                    </div>
                </div>

        <?php } ?>
        <?php }
        ?>